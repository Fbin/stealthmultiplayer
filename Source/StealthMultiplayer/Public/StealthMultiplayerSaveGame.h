// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "Types.h"
#include "StealthMultiplayerSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class STEALTHMULTIPLAYER_API UStealthMultiplayerSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:
	UPROPERTY(Replicated, BlueprintReadWrite)
		FPlayerInfo S_PlayerInfo;
};

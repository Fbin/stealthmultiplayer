// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "StealthMultiplayer.h"
#include "Types.h"
#include "KickPlayerUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class STEALTHMULTIPLAYER_API UKickPlayerUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite, Replicated)
		FPlayerInfo playerSettings;

	UPROPERTY(BlueprintReadWrite, Replicated)
		FString connectedPlayerName;

	UPROPERTY(BlueprintReadWrite, Replicated)
		UTexture2D* connectedPlayerIcon;

	UPROPERTY(BlueprintReadWrite, Replicated)
		int connectedPlayerID;
};

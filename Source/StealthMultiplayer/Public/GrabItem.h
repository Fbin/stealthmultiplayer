// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractableInterface.h"
#include "GrabItem.generated.h"

UCLASS()
class STEALTHMULTIPLAYER_API AGrabItem : public AActor, public IInteractableInterface
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* visualComponent;
	UPROPERTY(VisibleAnywhere)
		class USceneComponent* colliderComponentRoot;
	UPROPERTY(EditAnywhere)
		int strengthRequiredToMove;
	UPROPERTY(EditAnywhere)
		bool allowRotation;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		TArray<class UPrimitiveComponent*> grabColliderRefs;
	//TODO maybe use trace hitnormal instead
	UPROPERTY(EditAnywhere)
		TArray<bool> allowMoveForward;
	UPROPERTY(EditAnywhere)
		TArray<bool> allowMoveRight;
	UPROPERTY(Replicated, BlueprintReadOnly)
		AActor* grabbedBy;
	FRotator initRotation;
	UPROPERTY(Replicated)
		FVector forwardDirection;
	UPROPERTY(Replicated)
		FVector rightDirection;

public:	
	// Sets default values for this actor's properties
	AGrabItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Grab_Implementation(USceneComponent* attachTo) override;
	virtual void Release_Implementation() override;
	virtual void Interact_Implementation(AActor* user) override;
	FORCEINLINE int GetRequiredStrength() const { return strengthRequiredToMove; }
	void SetDirections();
	UFUNCTION(BlueprintCallable)
	FVector GetMovementForwardVector();
	UFUNCTION(BlueprintCallable)
		FVector GetMovementRightVector();
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Types.h"
#include "StealthMultiplayer.h"
#include "Blueprint/UserWidget.h"
#include "Components/VerticalBox.h"
#include "LobbyUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class STEALTHMULTIPLAYER_API ULobbyUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent)
		void ClearPlayerList();

	UFUNCTION(BlueprintImplementableEvent)
		void ShowPlayerList(UUserWidget* player);

	UFUNCTION(BlueprintCallable, Reliable, Client, WithValidation)
		void UpdatePlayerWindow(FPlayerInfo newPlayerInfo);

	UFUNCTION(BlueprintImplementableEvent)
		void OnUpdateAvailability(/*TArray<bool> characterAvailability*/);

	UPROPERTY(BlueprintReadWrite, Replicated)
		FText mapName;

	UPROPERTY(BlueprintReadWrite, Replicated)
		UTexture2D* mapImage;

	UUserWidget* playerRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
		TSubclassOf<class UUserWidget> wPlayer;
};

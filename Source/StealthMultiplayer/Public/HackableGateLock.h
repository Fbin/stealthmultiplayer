// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HackablePawn.h"
#include "Types.h"
#include "HackableGateLock.generated.h"

/**
 * 
 */
UCLASS()
class STEALTHMULTIPLAYER_API AHackableGateLock : public AHackablePawn
{
	GENERATED_BODY()
protected:
	UPROPERTY(VisibleAnywhere)
		class USceneComponent* lockHandlesRoot;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		TArray<class USceneComponent*> lockHandles;
	UPROPERTY(Replicated, BlueprintReadOnly)
	int activeHandleIndex;
	UPROPERTY(VisibleAnywhere, Replicated, BlueprintReadOnly)
		TArray<FVector> lockHandlePositions;
	UPROPERTY(EditAnywhere)
		TArray<FLockInfo> lockHandlesInfo;
	UPROPERTY(EditAnywhere)
		float moveSpeed = 1.0f;
	UPROPERTY(EditAnywhere)
		float handleMoveDelay = 0.5f;
	/*position of handles (relative to lockHandlesRoot)*/
	UPROPERTY(EditAnywhere)
		TArray<FVector> lockPassword;
	UPROPERTY(EditAnywhere)
		float errorZone = 1.0f;

protected:
	virtual void BeginPlay() override;
	/*used to move the active handle*/
	virtual void MoveForward(float value) override;
	/*used to move the active handle*/
	virtual void MoveRight(float value) override;
	virtual void InitiateRespossess() override;
	/*used to switch between active lockHandles*/
	virtual void InitiateHackOtherObject() override;
	UFUNCTION(Reliable, Server, WithValidation)
		void ServerUpdateActiveHandleIndex();
	UFUNCTION(Reliable, Server, WithValidation)
		void ServerUpdateLockHandlesPosition(FVector newLocation);
	UFUNCTION(Reliable, Client, WithValidation)
		void ClientUpdateLockHandlesPosition(bool passwordCorrect, FVector newLocation);
	UFUNCTION(Reliable, Server, WithValidation)
		void ServerTryUnlock(AActor* target);

private:
	bool CheckPassword();
	bool IsHandleInLimits(FVector newLocation, FVector direction);
	void EnableLockMovement();
	
public:
	AHackableGateLock();
	
};

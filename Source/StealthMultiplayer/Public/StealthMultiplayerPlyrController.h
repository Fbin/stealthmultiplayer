// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "StealthMultiplayerPlyrController.generated.h"

/**
 * 
 */
UCLASS()
class STEALTHMULTIPLAYER_API AStealthMultiplayerPlyrController : public APlayerController
{
	GENERATED_BODY()

protected:
	UPROPERTY(Replicated)
		TSubclassOf<APawn> MyPawnClass;
	
public:
	UFUNCTION(BlueprintImplementableEvent)
		void OnGameFinished(APawn* instigatorPawn, bool gameSuccessful);
	FORCEINLINE UClass* GetPlayerPawnClass() { return MyPawnClass; }

protected:
	virtual void BeginPlay() override;
	UFUNCTION(Reliable, Server, WithValidation)
		virtual void ServerSetPawn(TSubclassOf<APawn> InPawnClass);
	UFUNCTION(Reliable, Client)
		void DeterminePawnClass();
};

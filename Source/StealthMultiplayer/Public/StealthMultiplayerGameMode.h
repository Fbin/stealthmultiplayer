// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "StealthMultiplayerGameMode.generated.h"

UCLASS(minimalapi)
class AStealthMultiplayerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AStealthMultiplayerGameMode();

	void GameFinished(APawn* instigatorPawn, bool gameSuccessful);

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AActor> spectatingViewpointClass;

	virtual void RestartPlayerAtPlayerStart(AController* NewPlayer, AActor* StartSpot) override;

private:
	/*if is false, uses the selected character from menu*/
	UPROPERTY(EditDefaultsOnly)
		bool useDefaultClass;
};




// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "StealthMultiplayer.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionInterface.h"
#include "FindSessionsCallbackProxy.h"
#include "Types.h"
#include "StealthMultiplayerGameInstance.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnFindSessionsResultDelegate, const TArray<FBlueprintSessionResult>&, Results);

/**
 * 
 */
UCLASS()
class STEALTHMULTIPLAYER_API UStealthMultiplayerGameInstance : public UGameInstance
{
	GENERATED_BODY()

	UStealthMultiplayerGameInstance(const FObjectInitializer& ObjectInitializer);
	
	//create Session
	bool HostSession(const FUniqueNetId& UserId, FName SessionName, bool bIsLAN, bool bIsPresence, int32 MaxNumPlayers, FName MapName);

	/* Delegate called when session created */
	FOnCreateSessionCompleteDelegate OnCreateSessionCompleteDelegate;
	/* Delegate called when session started */
	FOnStartSessionCompleteDelegate OnStartSessionCompleteDelegate;

	/** Handles to registered delegates for creating/starting a session */
	FDelegateHandle OnCreateSessionCompleteDelegateHandle;
	FDelegateHandle OnStartSessionCompleteDelegateHandle;

	TSharedPtr<class FOnlineSessionSettings> SessionSettings;

	virtual void OnCreateSessionComplete(FName SessionName, bool bWasSuccessful);

	void OnStartOnlineGameComplete(FName SessionName, bool bWasSuccessful);

	//find Session
	void FindSessions(const FUniqueNetId& UserId, bool bIsLAN, bool bIsPresence);

	/** Delegate for searching for sessions */
	FOnFindSessionsCompleteDelegate OnFindSessionsCompleteDelegate;

	/** Handle to registered delegate for searching a session */
	FDelegateHandle OnFindSessionsCompleteDelegateHandle;

	TSharedPtr<class FOnlineSessionSearch> SessionSearch;

	void OnFindSessionsComplete(bool bWasSuccessful);

	//join Session
	bool JoinHostedSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, const FOnlineSessionSearchResult& SearchResult);

	/** Delegate for joining a session */
	FOnJoinSessionCompleteDelegate OnJoinSessionCompleteDelegate;

	/** Handle to registered delegate for joining a session */
	FDelegateHandle OnJoinSessionCompleteDelegateHandle;

	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

	//destroy Session

	/** Delegate for destroying a session */
	FOnDestroySessionCompleteDelegate OnDestroySessionCompleteDelegate;

	/** Handle to registered delegate for destroying a session */
	FDelegateHandle OnDestroySessionCompleteDelegateHandle;

	virtual void OnDestroySessionComplete(FName SessionName, bool bWasSuccessful);

	void EnableMouse();

	UUserWidget* MainMenuRef;
	UUserWidget* HostMenuRef;
	UUserWidget* ServerMenuRef;
	UUserWidget* LoadingScreenRef;
	UUserWidget* OptionsRef;
	UUserWidget* PlayerSettingsRef;
	UUserWidget* KickListRef;


public:
	UUserWidget* LobbyRef;
	UFUNCTION(BlueprintCallable, Category = "Network")
		void CreateSession(FName SessionName, bool bIsLAN, int32 MaxNumPlayers, FName MapName = "test");

	UFUNCTION(BlueprintCallable, Category = "Network")
		void StartSession();

	UFUNCTION(BlueprintCallable, Category = "Network")
		void FindOnlineGames(bool bIsLAN, TArray<FBlueprintSessionResult> &results);

	UFUNCTION(BlueprintCallable, Category = "Network")
		void JoinOnlineGame(FBlueprintSessionResult result);

	UFUNCTION(BlueprintCallable, Category = "Network")
		void DestroySessionAndLeaveGame(FBlueprintSessionResult result);

	UFUNCTION(BlueprintCallable, Category = "Show Menus")
		void ShowMainMenu();

	UFUNCTION(BlueprintCallable, Category = "Show Menus")
		void ShowHostMenu();

	UFUNCTION(BlueprintCallable, Category = "Show Menus")
		void ShowServerMenu();

	UFUNCTION(BlueprintCallable, Category = "Show Menus")
		void ShowLoadingScreen();

	UFUNCTION(BlueprintCallable, Category = "Show Menus")
		void ShowOptions();

	UFUNCTION(BlueprintCallable, Category = "Show Menus")
		void ShowPlayerSettings();

	UFUNCTION(BlueprintCallable, Category = "Show Menus")
		void ShowLobby();

	UFUNCTION(BlueprintCallable, Category = "Show Menus")
		void ShowKickList();

	UFUNCTION(BlueprintPure, Category = "Network|Session")
		static int32 GetPingInMs(const FBlueprintSessionResult& Result);

	UFUNCTION(BlueprintPure, Category = "Network|Session")
		static FString GetServerName(const FBlueprintSessionResult& Result);

	UFUNCTION(BlueprintPure, Category = "Network|Session")
		static int32 GetCurrentPlayers(const FBlueprintSessionResult& Result);

	UFUNCTION(BlueprintPure, Category = "Network|Session")
		static int32 GetMaxPlayers(const FBlueprintSessionResult& Result);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
		TSubclassOf<class UUserWidget> wMainMenu;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
		TSubclassOf<class UUserWidget> wHostMenu;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
		TSubclassOf<class UUserWidget> wServerMenu;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
		TSubclassOf<class UUserWidget> wLoadingScreen;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
		TSubclassOf<class UUserWidget> wOptions;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
		TSubclassOf<class UUserWidget> wPlayerSettings;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
		TSubclassOf<class UUserWidget> wLobby;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
		TSubclassOf<class UUserWidget> wKickList;

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings")
		UTexture2D* ChangeAvatar(int& currentID, int changeVal, TArray<UTexture2D*> avatars);

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|SaveGame")
		void SaveGame(FString saveGameSlotName/*, TSubclassOf<USaveGame> SaveGameClass*/, FPlayerInfo playerInfo);

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|SaveGame")
		FPlayerInfo LoadGame(FString saveGameSlotName);

	UFUNCTION(BlueprintPure, Category = "PlayerSettings|SaveGame")
		bool SaveGameCheck(FString saveGameSlotName);

	UFUNCTION(BlueprintCallable, Category = "PlayerSettings|SaveGame")
		FString GetSteamOrComputerName();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EFindState currentState = EFindState::Default;

	// Called when there is a successful query
	UPROPERTY(BlueprintAssignable)
		FOnFindSessionsResultDelegate OnSuccessDelegate;

	// Called when there is an unsuccessful query
	UPROPERTY(BlueprintAssignable)
		FOnFindSessionsResultDelegate OnFailureDelegate;
	UFUNCTION(BlueprintCallable)
		void OnSuccessSessionFound(TArray<FBlueprintSessionResult>& Results);
	UFUNCTION(BlueprintCallable)
		void OnFailureSessionFound(TArray<FBlueprintSessionResult>& Results);

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<FBlueprintSessionResult> temp_results;

	//TArray<FPlayerInfo> players;

	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
		TArray<APlayerController*> allPlayerController;*/

	UPROPERTY(BlueprintReadOnly)
		FName currentSessionName;
};

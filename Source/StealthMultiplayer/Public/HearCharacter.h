// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StealthMultiplayerCharacter.h"
#include "HearCharacter.generated.h"

/**
 * 
 */
UCLASS()
class STEALTHMULTIPLAYER_API AHearCharacter : public AStealthMultiplayerCharacter
{
	GENERATED_BODY()

protected:
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USplineComponent* spline;
	UPROPERTY(EditDefaultsOnly)
		UActorComponent* splineMeshTemplate;
	UPROPERTY(EditDefaultsOnly)
		UStaticMesh* staticSplineMesh;
	UPROPERTY(EditDefaultsOnly)
		UMaterialInterface* splineMeshMaterial;
	UPROPERTY(EditDefaultsOnly)
		TArray<TEnumAsByte<EObjectTypeQuery>> objectTypesToTrace;
	UPROPERTY(EditDefaultsOnly)
		TArray<AActor*> actorsToIgnore;
	TArray<struct FPredictProjectilePathPointData*> splinePoints;
	UPROPERTY(VisibleAnywhere)
		TArray<class USplineMeshComponent*> splineMeshes;
	bool drawAimLine;
	bool sonarIsActive;
	float currentOpacity = 1.0f;
	UPROPERTY(EditDefaultsOnly)
		float fadeModifier = 1.0f;
	
public:
	AHearCharacter();
	virtual void PrepareResetActiveAction1() override;

protected:
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	virtual void Tick(float deltaTime);
	virtual void ActiveAction1() override;
	virtual void ResetActiveAction1() override;
	virtual void ActiveAction2() override;
	virtual void ResetActiveAction2() override;
	UFUNCTION(Server, Reliable, WithValidation)
		void ServerShootProjectile();
};

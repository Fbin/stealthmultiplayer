// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CableActor.h"
#include "GrapplingHook.generated.h"

/**
 * 
 */
UCLASS()
class STEALTHMULTIPLAYER_API AGrapplingHook : public AActor
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditDefaultsOnly)
		float maxSegmentLength = 100.0f;

private:
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class UCableComponent* cableComponent;
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class UBoxComponent* boxComponent;
	FVector targetLocation;
	float grapplingHookSpeed;
	float maxCableLength;
	bool tickEnabled;
	bool hookHitSomething;
	AActor* otherActor;

public:
	AGrapplingHook();
	void Shoot(bool didHookHit, FVector newtargetLocation, float hookSpeed, float hookLength, AActor* initiator, FName ComponentProperty, FName SocketName = NAME_None);
	void CheckHookLength();
	void Place();
	UFUNCTION(Reliable, NetMulticast, WithValidation)
		void MulticastPlace(AActor* target);
	FORCEINLINE class UCableComponent* GetCableComponent() const { return cableComponent; }

protected:
	virtual void Tick(float deltaTime);

private:
	UFUNCTION(Reliable, NetMulticast, WithValidation)
		void MulticastAttachGrapplingHook(AActor* initiator, FName ComponentProperty, FName SocketName = NAME_None/*, FVector endLocation = FVector(0.0f, 0.0f, 0.0f)*/);
	UFUNCTION(Reliable, NetMulticast, WithValidation)
		void MulticastIncreaseNumSegments();
	
};

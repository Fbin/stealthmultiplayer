// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Types.h"
#include "StealthMultiplayer.h"
#include "Blueprint/UserWidget.h"
#include "Components/VerticalBox.h"
#include "KickListWidget.generated.h"

/**
 * 
 */
UCLASS()
class STEALTHMULTIPLAYER_API UKickListWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Reliable, Client, WithValidation)
		void UpdateWindow(const TArray<FPlayerInfo>& playersInfo);

	UUserWidget* kickButtonRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
		TSubclassOf<class UUserWidget> wPlayerKickButton;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
		UVerticalBox* kickListRef;
};

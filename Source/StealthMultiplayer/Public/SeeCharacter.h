// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StealthMultiplayerCharacter.h"
#include "SeeCharacter.generated.h"

/**
 * 
 */
UCLASS()
class STEALTHMULTIPLAYER_API ASeeCharacter : public AStealthMultiplayerCharacter
{
	GENERATED_BODY()
protected:
	UPROPERTY(BlueprintReadOnly, Replicated)
		bool isClimbing;
	/*max distance between capsule component and wall to be climbing*/
	UPROPERTY(EditDefaultsOnly)
		float maxClimbDistance = 10.0f;
	/*max angle of wall deviated from vertical to be still considered for climbing*/
	UPROPERTY(EditDefaultsOnly)
		float maxClimbWallBentAngle = 45.0f;
	/*max angle of view deviation while climbing without falling off*/
	UPROPERTY(EditDefaultsOnly)
		float maxClimbAngle = 45.0f;
	UPROPERTY(EditDefaultsOnly)
		float climbJumpForce = 840.0f;
	UPROPERTY(BlueprintReadOnly)
	FHitResult currentWall;
	UPROPERTY(EditDefaultsOnly)
		float hackingTraceLength = 200.0f;
	class AHackablePawn* hackingTarget;
	UPROPERTY(Replicated, BlueprintReadOnly)
	bool didUseWallJump;
	
public:
	ASeeCharacter();
	virtual void PrepareResetActiveAction2() override;

protected:
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	virtual void Tick(float deltaTime) override;
	virtual void MoveForward(float Val) override;
	virtual void MoveRight(float Val) override;
	virtual void ActiveAction1() override;
	virtual void ActiveAction2() override;
	virtual void ResetActiveAction2() override;
	void PrepareHackFailed();
	void HackFailed();
	bool GroundCheck();
	/*offsetZ determines how much offset along the Z-axis the trace should be done, additionalLength determines if maxClimbAngle should be taken in consideration for trace length*/
	bool LedgeCheck(float offsetZ = 0.0f, bool additionalLength = false);
	UFUNCTION(Reliable, Server, WithValidation)
		void ServerPossess(AController* controllerForPossess, APawn* target);
	UFUNCTION(Reliable, Server, WithValidation)
		void ServerSetIsClimbing(bool newClimbing);
	UFUNCTION(Server, Reliable, WithValidation)
		void ServerJump(FVector direction, float force);
};

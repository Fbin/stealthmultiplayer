// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractableInterface.h"
#include "Types.h"
#include "Door.generated.h"

UCLASS()
class STEALTHMULTIPLAYER_API ADoor : public AActor, public IInteractableInterface
{
	GENERATED_BODY()

protected:
	UPROPERTY(VisibleAnywhere)
		class USceneComponent* root;
	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* mainDoor;
	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* secondDoor;
	UPROPERTY(VisibleAnywhere)
		class UBoxComponent* unlockColliderComponent;
	UPROPERTY(EditAnywhere)
		EDoorType doorOpenType;
	FVector mainDefaultLocation;
	FVector secondDefaultLocation;
	FRotator mainDefaultRotation;
	FRotator secondDefaultRotation;
	UPROPERTY(EditAnywhere)
		FVector targetOffsetLocationMain;
	UPROPERTY(EditAnywhere)
		FVector targetOffsetLocationSecond;
	UPROPERTY(EditAnywhere)
		FRotator targetOffsetRotationMain;
	UPROPERTY(EditAnywhere)
		FRotator targetOffsetRotationSecond;
	UPROPERTY(EditAnywhere)
		float openingTime = 2.0f;
	UPROPERTY(EditAnywhere)
		TArray<AActor*> locks;
	UPROPERTY(EditAnywhere)
		AActor* key;
	UPROPERTY(Replicated, BlueprintReadOnly)
		bool isOpening;
	UPROPERTY(Replicated, BlueprintReadOnly)
		float currentTime = openingTime + 0.1f;
	UPROPERTY(Replicated, BlueprintReadOnly)
		bool isLocked;
	
public:	
	// Sets default values for this actor's properties
	ADoor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool GetIsLocked();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Interact_Implementation(AActor* user) override;
	virtual void Unlock_Implementation(bool unlockInteractable) override;
	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

#if WITH_EDITOR
	virtual bool CanEditChange(const UProperty* InProperty) const override;
#endif
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StealthMultiplayerCharacter.h"
#include "SpeakCharacter.generated.h"

/**
 * 
 */
UCLASS()
class STEALTHMULTIPLAYER_API ASpeakCharacter : public AStealthMultiplayerCharacter
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadOnly, Replicated)
	class AGrapplingHook* grapplingHook;

private:
	/*UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class UCableComponent* cableComponent;*/

	/*speed of grappling hook in units per second*/
	UPROPERTY(EditDefaultsOnly)
		float grapplingHookSpeed = 200.0f;

	UPROPERTY(EditDefaultsOnly)
		float maxCableLength = 1000.0f;

	//bool isGrapplingHookActive;
	UPROPERTY(Replicated)
		bool canReactivateGrapplingHook;
	UPROPERTY(Replicated)
		bool isGrapplingHookReelingIn;
	/*UPROPERTY(Replicated, BlueprintReadOnly)
		FVector grappleHitLocation;*/
	UFUNCTION(Reliable, Server, WithValidation)
		void ServerSpawnGrapplingHook();
	UFUNCTION(Reliable, Server, WithValidation)
		void ServerPlaceGrapplingHook();
	UFUNCTION(Reliable, Server, WithValidation)
		void ServerReelGrapplingHook();
	UFUNCTION(Reliable, Server, WithValidation)
		void ServerReelGrapplingHookFinished();
	UFUNCTION(Reliable, NetMulticast, WithValidation)
		void MulticastMovementComponentReelIn(bool doesReelIn);
	/*which grappling hook to spawn*/
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		TSubclassOf<class AGrapplingHook> grapplingHookClass;

public:
	ASpeakCharacter();
	void SetGrapplingHookReadyForReelingIn();
	UPROPERTY(Replicated, BlueprintReadOnly)
		FVector grappleHitLocation;

protected:
	//virtual void BeginPlay() override;
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	virtual void Tick(float deltaTime);
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void ActiveAction1() override;
	virtual void ResetActiveAction1() override;
	virtual void ActiveAction2() override;
	virtual void ResetActiveAction2() override;
	/*UFUNCTION()
		void RespondToHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);*/

private:
	/*UFUNCTION()
		void ReelGrapplingHook();*/

};

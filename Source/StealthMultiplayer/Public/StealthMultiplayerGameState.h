// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "StealthMultiplayerGameState.generated.h"

/**
 * 
 */
UCLASS()
class STEALTHMULTIPLAYER_API AStealthMultiplayerGameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	UFUNCTION(NetMulticast, Reliable)
		void MulticastOnGameFinished(APawn* instigatorPawn, bool gameSuccessful);
};

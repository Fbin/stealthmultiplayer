// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Types.h"
#include "StealthMultiplayerCharacter.generated.h"

class UInputComponent;

UCLASS(config=Game)
class AStealthMultiplayerCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	class USkeletalMeshComponent* Mesh1P;

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* FP_Gun;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* FP_MuzzleLocation;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = Sound)
		class UPawnNoiseEmitterComponent* pawnNoiseEmitterComponent;

protected:
	bool isDefaultClass = true;
	UPROPERTY(Replicated)
		bool canTurnBody = true;

public:
	AStealthMultiplayerCharacter();

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float deltaTime) override;

public:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector GunOffset;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category=Projectile)
	TSubclassOf<class AStealthMultiplayerProjectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	class USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class UAnimMontage* FireAnimation;

	/*contains info about the object and amount*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay)
		TMap<EPickupItems, FInventory> inventory;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated)
		bool canUseActiveAction1 = true;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated)
		bool canUseActiveAction2 = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float activeAction1ActiveTime = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float activeAction2ActiveTime = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float activeAction1ReuseDelay = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float activeAction2ReuseDelay = 1.0f;

	FTimerHandle resetActiveAction1Handle;
	FTimerHandle resetActiveAction2Handle;

	UPROPERTY(EditDefaultsOnly)
		float maxUseDistance = 200.0f;

protected:
	
	/** Fires a projectile. */
	void OnFire();

	UFUNCTION(Server, Reliable, WithValidation)
		void ServerOnFire();

	/** Handles moving forward/backward */
	virtual void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	virtual void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface
	/*interaction with surrounding like opening doors/chests, picking up things or character special action*/
	AActor* GetObjectInView();
	virtual void Interact();
	UFUNCTION(Server, Reliable, WithValidation)
		void ServerInteract(AActor* target);
	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "ActiveAction1"))
		void ReceiveActiveAction1();
	UFUNCTION(Server, Reliable, WithValidation)
		void ServerActiveAction1();
	virtual void ActiveAction1();
	UFUNCTION(Server, Reliable, WithValidation)
		void ServerResetActiveAction1();
	virtual void ResetActiveAction1();
	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "ActiveAction2"))
		void ReceiveActiveAction2();
	UFUNCTION(Server, Reliable, WithValidation)
		void ServerActiveAction2();
	virtual void ActiveAction2();
	UFUNCTION(Server, Reliable, WithValidation)
		void ServerResetActiveAction2();
	virtual void ResetActiveAction2();
	UFUNCTION(NetMulticast, Reliable)
		void MulticastUpdateCanTurnBody(bool newState);
	/*reference asset to find world instance on begin play*/
	UPROPERTY(EditDefaultsOnly)
		UMaterialParameterCollection* materialParameterCollectionRef;
	UMaterialParameterCollectionInstance* materialParameterCollection;
	UPROPERTY(EditDefaultsOnly)
		float initialProjectileSpeed = 3000.0f;
	UPROPERTY(Replicated)
		AActor* grabbedObject;
	UPROPERTY(EditDefaultsOnly)
		int strength;

public:
	virtual void PrepareResetActiveAction1();
	virtual void PrepareResetActiveAction2();
	UFUNCTION(Server, Reliable, WithValidation)
		void ServerUnlockTarget(AActor* target, bool newLockState);
	UFUNCTION(NetMulticast, Reliable)
		void MulticastUpdateInventory(EPickupItems newItemType, FInventory newItemData);
	/*prevents character body to rotate but still allows the player to turn the camera to keep track of surrounding*/
	UFUNCTION(Server, Reliable, WithValidation)
		void ServerUpdateCanTurnBody(bool newState);
	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }
	/** Returns MuzzleLocation subobject **/
	FORCEINLINE class USceneComponent* GetFP_MuzzleLocation() const { return FP_MuzzleLocation; }
	FORCEINLINE bool GetIsDefaultClass() const { return isDefaultClass; }
	void SetGrabbedObject(AActor* newGrabbedObject);
};


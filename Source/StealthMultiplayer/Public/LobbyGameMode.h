// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "Types.h"
#include "StealthMultiplayerPlyrController.h"
#include "LobbyPlayerController.h"
#include "LobbyGameMode.generated.h"

/**
 * 
 */
UCLASS()
class  STEALTHMULTIPLAYER_API ALobbyGameMode : public AGameMode
{
	GENERATED_BODY()

		ALobbyGameMode(const FObjectInitializer& ObjectInitializer);
	
public:
	UPROPERTY(Replicated, BlueprintReadOnly)
		TArray<FPlayerInfo> players;

	UPROPERTY(Replicated)
		int currentPlayerAmount;

	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exiting) override;
	
	UFUNCTION(BlueprintCallable)
		void SetPlayerCharacter(UClass* selectedCharacter, int index);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool CheckAllReady();

	/*UFUNCTION(BlueprintCallable)
		void SetPlayerWeapon(AWeapon* selectedWeapon, int index);*/

	/*UFUNCTION(BlueprintCallable)
		void SetPlayerItem(AItem* selectedItem, int index);*/

	void SwapCharacter(APlayerController* playerController, TSubclassOf<ACharacter> newCharacter, bool changedStatus);

	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation)
		void ServerSwapCharacter(APlayerController* playerController, TSubclassOf<ACharacter> newCharacter, bool changedStatus);

	void UpdateAll();

	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation)
		void ServerUpdateAll();

	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation)
		void ServerUpdateGameSettings(UTexture2D* mapImage, const FText& mapName, int mapId);

	void AddToKickList();
	
	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation)
		void ServerKickPlayer(int playerId);

	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation)
		void RespawnPlayer(APlayerController* pcRef);

	/*used to determine if each player has selected a character != default*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool CanStart;

	UPROPERTY(EditDefaultsOnly, Category = "Defaults")
		TSubclassOf<AActor> startPointClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
		TArray<AActor*> startPoints;

	UPROPERTY(Replicated, EditDefaultsOnly)
		UTexture2D* gMMapImage;

	UPROPERTY(Replicated, EditDefaultsOnly)
		FText gMMapName;

	UPROPERTY(Replicated)
		int gMMapId;

	/*needs to be set to match the available character amount*/
	UPROPERTY(Replicated, BlueprintReadWrite, EditAnywhere)
		TArray<bool> characterAvailability;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
		TArray<APlayerController*> allPlayerController;
};

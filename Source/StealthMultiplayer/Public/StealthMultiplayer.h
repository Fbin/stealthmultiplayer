// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UnrealNetwork.h"
#include "CoreOnline.h"

DECLARE_LOG_CATEGORY_EXTERN(LobbyLog, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(StealthIntanceLog, Log, All);
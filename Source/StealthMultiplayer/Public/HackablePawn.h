// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "InteractableInterface.h"
#include "HackablePawn.generated.h"

UCLASS()
class STEALTHMULTIPLAYER_API AHackablePawn : public APawn, public IInteractableInterface
{
	GENERATED_BODY()

public:
	FTimerHandle hackingHandle;

private:
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USphereComponent* SphereComponent;
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class UStaticMeshComponent* Mesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* CameraComponent;
	UPROPERTY(VisibleAnywhere)
		class UBoxComponent* unlockColliderComponent;
	class ASeeCharacter* hackerRef;
	UPROPERTY(EditDefaultsOnly)
		class UMaterialParameterCollection* materialParameterCollectionRef;
	class UMaterialParameterCollectionInstance* materialParameterCollection;
	float currentUpAngle;
	float currentRightAngle;
	UPROPERTY(EditDefaultsOnly)
		float maxUpAngle = 90.0f;
	UPROPERTY(EditDefaultsOnly)
		float maxDownAngle = 90.0f;
	UPROPERTY(EditDefaultsOnly)
		float maxRightAngle = 90.0f;
	UPROPERTY(EditDefaultsOnly)
		float maxLeftAngle = 90.0f;

protected:
	bool isHacking;
	UPROPERTY(EditDefaultsOnly)
		float hackingTraceLength = 1000.0f;
	class AHackablePawn* hackingTarget;
	UPROPERTY(EditAnywhere)
		TArray<AActor*> locks;
	/*value only changes when entering unlock area with correct key*/
	UPROPERTY(EditAnywhere, Replicated)
		bool isLocked;
	/*reference on moveable object to place at unlock area*/
	UPROPERTY(EditAnywhere)
		AActor* key;

public:
	// Sets default values for this pawn's properties
	AHackablePawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void MoveForward(float value);
	virtual void MoveRight(float value);
	virtual void InitiateRespossess();
	virtual void RepossessPlayer();
	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "HackOtherObject"))
		void ReceiveHackOtherObject();
	virtual void InitiateHackOtherObject();
	virtual void HackOtherObject();
	UFUNCTION(Reliable, Server, WithValidation)
		void ServerPossess(AController * controllerForPossess, APawn * target);
	UFUNCTION(Server, Reliable, WithValidation)
		void ServerUnlock(bool newUnlockState);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void SetHacker(class ASeeCharacter* newHacker);
	FORCEINLINE class USphereComponent* GetSphereComponent() const { return SphereComponent; }
	FORCEINLINE class UStaticMeshComponent* GetMesh() const { return Mesh; }
	FORCEINLINE class UCameraComponent* GetCameraComponent() const { return CameraComponent; }
	virtual void Unlock_Implementation(bool unlockInteractable) override;
	virtual bool GetIsUnlocked_Implementation() override;
	bool CanBeHacked();
	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};

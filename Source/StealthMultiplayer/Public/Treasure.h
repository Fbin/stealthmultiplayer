#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractableInterface.h"
#include "Types.h"
#include "Treasure.generated.h"

UCLASS()
class STEALTHMULTIPLAYER_API ATreasure : public AActor, public IInteractableInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATreasure();

protected:
	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* visualComponent;
	UPROPERTY(VisibleAnywhere)
		class USphereComponent* colliderComponent;
	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* pickupEffect;
	UPROPERTY(EditAnywhere)
		int amount = 1;
	/*in case of a key, reference to the gate it unlocks*/
	UPROPERTY(EditAnywhere)
		AActor* gateRef;
	UPROPERTY(EditAnywhere)
		EPickupItems itemType;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void PlayEffects();

public:	
	//virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	virtual void PickUp_Implementation(AActor* user) override;
	virtual void Drop_Implementation() override;
	virtual void Interact_Implementation(AActor* user) override;
	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};

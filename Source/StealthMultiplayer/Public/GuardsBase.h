// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
//#include "GameFramework/Pawn.h"
#include "GameFramework/Character.h"
#include "Types.h"
#include "GuardsBase.generated.h"

UCLASS()
class STEALTHMULTIPLAYER_API AGuardsBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AGuardsBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
		class UPawnSensingComponent* pawnSensingComponent;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		class UWidgetComponent* widgetComponent;

	/*needs at least the default position and rotation of the actor*/
	UPROPERTY(EditInstanceOnly)
		TArray</*AActor**/FPatrolInfo> patrolPoints;
	UPROPERTY(EditDefaultsOnly)
		float allowedDistanceToPatrolPoints;

	UPROPERTY(ReplicatedUsing=OnRep_GuardState)
		EGuardState guardState;
	UPROPERTY(EditAnywhere)
		EGuardType guardType;
	UPROPERTY(EditAnywhere)
		bool killOnSight;

	FTimerHandle timerHandle_ResetOrientation;
	FTimerHandle timerHandle_PatrolDelay;
	/*time after reaction until reset to originial action/position*/
	UPROPERTY(EditAnywhere)
		float resetTime = 3.0f;
	int currentPatrolPoint;
	bool patrolUp = true;
	FVector targetLocation;
	/*degree per second*/
	UPROPERTY(EditDefaultsOnly)
		float rotationSpeed = 90.0f;
	float rotationTime;
	FRotator targetRotation;
	UPROPERTY(EditAnywhere)
		float roamingRadius = 1000.0f;
	float minSeeDistanceToSuspicious = 4000.0f;
	float minSeeDistanceToSearching = 2000.0f;
	float sightDistanceDefault;
	UPROPERTY(EditAnywhere)
		float sightDistanceUnderSuspicion = 10000.0f;
	UPROPERTY(EditAnywhere)
		float sightDistanceUnderSearching = 15000.0f;
	UPROPERTY(EditAnywhere)
		float minSeeDistanceToSuspiciousDefault = 4000.0f;
	UPROPERTY(EditAnywhere)
		float minSeeDistanceToSuspiciousUnderSuspicion = 5000.0f;
	UPROPERTY(EditAnywhere)
		float minSeeDistanceToSearchingDefault = 2000.0f;
	UPROPERTY(EditAnywhere)
		float minSeeDistanceToSearchingUnderSuspicion = 4000.0f;
	UPROPERTY(EditAnywhere)
		float minSeeDistanceToSearchingUnderSearching = 5000.0f;
	float hearingDistanceDefault;
	UPROPERTY(EditAnywhere)
		float hearingDistanceUnderSuspicion = 2500.0f;
	UPROPERTY(EditAnywhere)
		float hearingDistanceUnderSearching = 6000.0f;
	float losHearingDistanceDefault;
	UPROPERTY(EditAnywhere)
		float losHearingDistanceUnderSuspicion = 5000.0f;
	UPROPERTY(EditAnywhere)
		float losHearingDistanceUnderSearching = 12000.0f;
	UPROPERTY(EditAnywhere)
		int maxSearchPoints = 5;
	int searchCounter;
	UPROPERTY(EditAnywhere)
		float searchingRadius = 5000.0f;
	FVector searchCenter;

	class AAIController* aiControllerRef;

	UFUNCTION()
		virtual void OnSeePawn(APawn* seenPawn);
	virtual void SeeReaction(APawn* seenPawn, EGuardState newState);

	UFUNCTION()
		virtual void OnNoiseHeard(APawn* heardPawn, const FVector& location, float volume);
	virtual void NoiseReaction(const FVector& location, EGuardState newState);
	
	/*used to get guard back to patroling*/
	UFUNCTION()
		void ResetOrientation();

	UFUNCTION(BlueprintImplementableEvent)
		void OnUpdateGuardState(EGuardState newState);

	UFUNCTION()
		void OnRep_GuardState();

	void SetGuardState(EGuardState newState);

	void Patrol();
	bool ReachedDestination();
	bool ReachedRotation();
	void Rotate(float deltaTime);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};

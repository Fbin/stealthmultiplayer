#pragma once

#include "Types.generated.h"

UENUM(BlueprintType)
enum class EFindState : uint8
{
	OnSuccess,

	OnFailure,

	Default,
};

UENUM(BlueprintType)
enum class EDoorType : uint8
{
	Swing,
	Slide
};

UENUM(BlueprintType)
enum class EPickupItems : uint8
{
	Default,
	Treasure,
	Treasure2,
	Treasure3,
};

UENUM(BlueprintType)
enum class EGuardType : uint8 {
	Stationary,
	PatrolLinear,
	PatrolCircular,
	Roaming
};

UENUM(BlueprintType)
enum class EGuardState : uint8 {
	GuardState_Idle,
	GuardState_Suspicious,
	GuardState_Alert,
	GuardState_Searching
};

USTRUCT(BlueprintType)
struct FPlayerInfo
{
	GENERATED_USTRUCT_BODY()

		FPlayerInfo() {
	}

	UPROPERTY(BlueprintReadWrite)
		FString playerName;

	UPROPERTY(BlueprintReadWrite)
		UTexture2D* playerIcon;

	UPROPERTY(BlueprintReadWrite)
		UClass* playerCharacter;

	UPROPERTY(BlueprintReadWrite)
		UTexture2D* playerCharacterImage;

	//UPROPERTY(BlueprintReadWrite)
	//	class AWeapon* standardWeapon;

	//UPROPERTY(BlueprintReadWrite)
	//	class AItem* standardItem;

	UPROPERTY(BlueprintReadWrite)
		bool isReady;
};

USTRUCT(BlueprintType)
struct FLockInfo
{
	GENERATED_USTRUCT_BODY()

	FLockInfo() {
		lockHandleMoveDirectionForward = FVector::ForwardVector;
		lockHandleMoveDirectionRight = FVector::RightVector;
		handleLimitMin = FVector(-5.0f, 0.0f, 0.0f);
		handleLimitMax = FVector(5.0f, 0.0f, 0.0f);
	}

	UPROPERTY(EditAnywhere)
		FVector lockHandleMoveDirectionForward;

	UPROPERTY(EditAnywhere)
		FVector lockHandleMoveDirectionRight;

	/*x is limit for handleMoveForward, y is limit for handleMoveRight*/
	UPROPERTY(EditAnywhere)
		FVector handleLimitMin;

	/*x is limit for handleMoveForward, y is limit for handleMoveRight*/
	UPROPERTY(EditAnywhere)
		FVector handleLimitMax;
};

USTRUCT(BlueprintType)
struct FPatrolInfo
{
	GENERATED_USTRUCT_BODY()

	FPatrolInfo() {}

	FPatrolInfo(FVector inLocation)
		: location(inLocation)
	{}

	UPROPERTY(EditAnywhere)
		FVector location;

	UPROPERTY(EditAnywhere)
		FRotator rotation;

	UPROPERTY(EditAnywhere)
		float waitingTime;
};

USTRUCT(BlueprintType)
struct FInventory
{
	GENERATED_USTRUCT_BODY()

		FInventory() {}

	FInventory(UClass* inClassRef, UStaticMeshComponent* inVisualComponent, class USphereComponent* inColliderComponent, UParticleSystem* inPickupEffect)
		: classRef(inClassRef)
		, visualComponent(inVisualComponent)
		, colliderComponent(inColliderComponent)
		, pickupEffect(inPickupEffect)
		, gateRef(nullptr)
		, amount(1)
	{}

	FInventory(UClass* inClassRef, UStaticMeshComponent* inVisualComponent, class USphereComponent* inColliderComponent, UParticleSystem* inPickupEffect, int inAmount)
		: classRef(inClassRef)
		, visualComponent(inVisualComponent)
		, colliderComponent(inColliderComponent)
		, pickupEffect(inPickupEffect)
		, gateRef(nullptr)
		, amount(inAmount)
	{}

	FInventory(UClass* inClassRef, UStaticMeshComponent* inVisualComponent, class USphereComponent* inColliderComponent, UParticleSystem* inPickupEffect, AActor* inGateRef)
		: classRef(inClassRef)
		, visualComponent(inVisualComponent)
		, colliderComponent(inColliderComponent)
		, pickupEffect(inPickupEffect)
		, gateRef(inGateRef)
		, amount(1)
	{}

	FInventory(UClass* inClassRef, UStaticMeshComponent* inVisualComponent, class USphereComponent* inColliderComponent, UParticleSystem* inPickupEffect, AActor* inGateRef, int inAmount)
		: classRef(inClassRef)
		, visualComponent(inVisualComponent)
		, colliderComponent(inColliderComponent)
		, pickupEffect(inPickupEffect)
		, gateRef(inGateRef)
		, amount(inAmount)
	{}

	UPROPERTY(BlueprintReadWrite)
		UClass* classRef;
	UPROPERTY(BlueprintReadWrite)
		UStaticMeshComponent* visualComponent;
	UPROPERTY(BlueprintReadWrite)
		class USphereComponent* colliderComponent;
	UPROPERTY(BlueprintReadWrite)
		UParticleSystem* pickupEffect;
	/*in case it is a key*/
	UPROPERTY(BlueprintReadWrite)
		AActor* gateRef;
	UPROPERTY(BlueprintReadWrite)
		int amount;
};
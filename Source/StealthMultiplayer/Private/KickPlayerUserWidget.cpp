// Fill out your copyright notice in the Description page of Project Settings.

#include "KickPlayerUserWidget.h"

void UKickPlayerUserWidget::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to every client, no special condition required
	DOREPLIFETIME(UKickPlayerUserWidget, playerSettings);
	DOREPLIFETIME(UKickPlayerUserWidget, connectedPlayerName);
	DOREPLIFETIME(UKickPlayerUserWidget, connectedPlayerIcon);
}
// Fill out your copyright notice in the Description page of Project Settings.

#include "StealthMultiplayerGameState.h"
#include "StealthMultiplayerPlyrController.h"

void AStealthMultiplayerGameState::MulticastOnGameFinished_Implementation(APawn* instigatorPawn, bool gameSuccessful) {
	for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; it++) {
		AStealthMultiplayerPlyrController* playerController = Cast<AStealthMultiplayerPlyrController>(it->Get());

		if (playerController && playerController->IsLocalController()) {
			playerController->OnGameFinished(instigatorPawn, gameSuccessful);
			APawn* pawn = playerController->GetPawn();

			if (pawn) {
				pawn->DisableInput(playerController);
			}
		}
	}
}
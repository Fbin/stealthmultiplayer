// Fill out your copyright notice in the Description page of Project Settings.

#include "StealthMultiplayerPlyrController.h"
#include "StealthMultiplayerCharacter.h"
#include "StealthMultiplayerGameInstance.h"
#include "Engine/World.h"
#include "GameFramework/GameModeBase.h"

void AStealthMultiplayerPlyrController::BeginPlay()
{
	Super::BeginPlay();

	DeterminePawnClass();
}

void AStealthMultiplayerPlyrController::ServerSetPawn_Implementation(TSubclassOf<APawn> InPawnClass)
{
	MyPawnClass = InPawnClass;

	GetWorld()->GetAuthGameMode()->RestartPlayer(this);
}

bool AStealthMultiplayerPlyrController::ServerSetPawn_Validate(TSubclassOf<APawn> InPawnClass)
{
	return true;
}

void AStealthMultiplayerPlyrController::DeterminePawnClass_Implementation()
{
	if (IsLocalController()) //Only Do This Locally (NOT Client-Only, since Server wants this too!)
	{
		UStealthMultiplayerGameInstance* giRef = Cast<UStealthMultiplayerGameInstance>(GetGameInstance());
		if (giRef->SaveGameCheck("PlayerSettingsSave"))
		{
			FPlayerInfo loadedInfo = giRef->LoadGame("PlayerSettingsSave");
			
			ServerSetPawn(loadedInfo.playerCharacter);
			//AActor* spawn = GetWorld()->SpawnActor(loadedInfo.playerCharacter, &location, &rotation, spawnParameters);
		}
	}
}

void AStealthMultiplayerPlyrController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	DOREPLIFETIME(AStealthMultiplayerPlyrController, MyPawnClass);
}
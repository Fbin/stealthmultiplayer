// Fill out your copyright notice in the Description page of Project Settings.

#include "SpeakCharacter.h"
//#include "CableComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GrapplingHook.h"
#include "Engine/World.h"
#include "UnrealNetwork.h"

void ASpeakCharacter::ServerSpawnGrapplingHook_Implementation()
{
	if (grapplingHook != nullptr) grapplingHook->Destroy();

	FHitResult hitResult;
	FVector start = /*cableComponent*/GetMesh()->GetComponentLocation();
	FVector end = start + GetFirstPersonCameraComponent()->GetForwardVector() * maxCableLength;
	ECollisionChannel traceChannel = ECollisionChannel::ECC_Visibility;
	GetWorld()->LineTraceSingleByChannel(hitResult, start, end, traceChannel);

	if (hitResult.bBlockingHit) grappleHitLocation = hitResult.Location;
	else grappleHitLocation = end;

	FActorSpawnParameters spawnParameters;
	//spawnParameters.Owner = this;
	spawnParameters.bNoFail = false;
	spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
	grapplingHook = GetWorld()->SpawnActor<AGrapplingHook>(grapplingHookClass, GetMesh()->GetComponentLocation(), GetMesh()->GetComponentRotation(), spawnParameters);
	grapplingHook->Shoot(hitResult.bBlockingHit, grappleHitLocation, grapplingHookSpeed, maxCableLength, this, TEXT("Mesh"));
}

bool ASpeakCharacter::ServerSpawnGrapplingHook_Validate()
{
	return true;
}

void ASpeakCharacter::ServerPlaceGrapplingHook_Implementation()
{
	canReactivateGrapplingHook = false;
	grapplingHook->Place();
}

bool ASpeakCharacter::ServerPlaceGrapplingHook_Validate()
{
	return true;
}

void ASpeakCharacter::ServerReelGrapplingHook_Implementation()
{
	isGrapplingHookReelingIn = true;
	FVector dir = grappleHitLocation - GetActorLocation();
	dir.Normalize();
	LaunchCharacter(dir * grapplingHookSpeed, false, false);
	canReactivateGrapplingHook = false;
	MulticastMovementComponentReelIn(true);
}

bool ASpeakCharacter::ServerReelGrapplingHook_Validate()
{
	return true;
}

void ASpeakCharacter::ServerReelGrapplingHookFinished_Implementation()
{
	isGrapplingHookReelingIn = false;
	MulticastMovementComponentReelIn(false);
	grapplingHook->Destroy();
	PrepareResetActiveAction1();
}

bool ASpeakCharacter::ServerReelGrapplingHookFinished_Validate()
{
	return true;
}

void ASpeakCharacter::MulticastMovementComponentReelIn_Implementation(bool doesReelIn)
{
	if (doesReelIn) {
		GetCharacterMovement()->GravityScale = 0.0f;
		GetCharacterMovement()->Velocity = FVector(0.0f, 0.0f, 0.0f);
		GetCharacterMovement()->AirControl = 0.0f;
	}
	else {
		GetCharacterMovement()->GravityScale = 1.0f;
		GetCharacterMovement()->AirControl = 0.05f;
	}
}

bool ASpeakCharacter::MulticastMovementComponentReelIn_Validate(bool doesReelIn)
{
	return true;
}

ASpeakCharacter::ASpeakCharacter() {
	isDefaultClass = false;
	/*cableComponent = CreateDefaultSubobject<UCableComponent>(TEXT("CableComponent"));
	cableComponent->EndLocation = FVector(0.0f, 0.0f, 0.0f);
	cableComponent->CableLength = 1.0f;
	cableComponent->CableWidth = 5.0f;
	cableComponent->SetVisibility(false);
	cableComponent->SetupAttachment(GetMesh());*/
	//GetCapsuleComponent()->OnComponentHit.AddDynamic(this, &ASpeakCharacter::RespondToHit);
}

void ASpeakCharacter::SetGrapplingHookReadyForReelingIn()
{
	canReactivateGrapplingHook = true;
}

void ASpeakCharacter::SetupPlayerInputComponent(UInputComponent * InputComponent)
{
	check(InputComponent);

	Super::SetupPlayerInputComponent(InputComponent);

}

void ASpeakCharacter::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	if (GetWorld()->IsServer()) {
		if (grapplingHook != nullptr) {
			//adjusts length of hook or destroys it when too long
			if (canReactivateGrapplingHook) grapplingHook->CheckHookLength();
		}

		if (isGrapplingHookReelingIn) {
			//lerp player to grappleHitLocation
			/*float grappleDelay = (GetActorLocation() - grappleHitLocation).Size() / grapplingHookSpeed;
			FVector newLocation = FMath::VInterpTo(GetActorLocation(), grappleHitLocation, deltaTime, 1.0f / grappleDelay);
			SetActorLocation(newLocation);*/

			FVector distance = (grappleHitLocation - GetActorLocation()).GetAbs();
			float heightLeft = distance.Z - GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
			float distanceLeft = distance.Size2D() - GetCapsuleComponent()->GetScaledCapsuleRadius();

			if (heightLeft < 1.0f && distanceLeft < 1.0f) {
				ServerReelGrapplingHookFinished();
			}
		}
	}
}

void ASpeakCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	//GetWorldTimerManager().ClearTimer(grappleTimerHandle);
}

void ASpeakCharacter::ActiveAction1()
{
	if (canReactivateGrapplingHook) {
		ServerReelGrapplingHook();
	}
	if (canUseActiveAction1) {
		Super::ActiveAction1();

		ServerSpawnGrapplingHook();
		//grapplingHook->GetCableComponent()->SetAttachEndTo(this, TEXT("Mesh"), NAME_None);
	}
}

void ASpeakCharacter::ResetActiveAction1()
{
	Super::ResetActiveAction1();
	//isGrapplingHookActive = false;
	//grappleDelay = 0.0f;
	/*cableComponent->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	cableComponent->SetVisibility(false);*/
}

//void ASpeakCharacter::ReelGrapplingHook()
//{
//	isGrapplingHookActive = false;
//	GetWorldTimerManager().ClearTimer(grappleTimerHandle);
//	isGrapplingHookReelingIn = true;
//	GetWorldTimerManager().SetTimer(resetActiveActionHandle, this, &ASpeakCharacter::ResetActiveAction, grappleDelay + activeActionReuseDelay, false);
//}

void ASpeakCharacter::ActiveAction2()
{
	//TODO only usable when player standing on ground
	if (canReactivateGrapplingHook) {
		Super::ActiveAction2();
		ServerPlaceGrapplingHook();
	}
}

void ASpeakCharacter::ResetActiveAction2()
{
	Super::ResetActiveAction2();

}

//void ASpeakCharacter::RespondToHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit)
//{
//	if (isGrapplingHookReelingIn) {
//		isGrapplingHookReelingIn = false;
//		GetCharacterMovement()->GravityScale = 1.0f;
//		grapplingHook->Destroy();
//		PrepareResetActiveAction1();
//	}
//}

void ASpeakCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASpeakCharacter, grapplingHook);
	DOREPLIFETIME(ASpeakCharacter, canReactivateGrapplingHook);
	DOREPLIFETIME(ASpeakCharacter, isGrapplingHookReelingIn);
	DOREPLIFETIME(ASpeakCharacter, grappleHitLocation);
}
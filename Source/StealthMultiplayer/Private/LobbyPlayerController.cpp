// Fill out your copyright notice in the Description page of Project Settings.

#include "LobbyPlayerController.h"
#include "StealthMultiplayer.h"
#include "StealthMultiplayerCharacter.h"
#include "StealthMultiplayerGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "LobbyGameMode.h"
#include "LobbyUserWidget.h"

ALobbyPlayerController::ALobbyPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	playerSettingsSaveSlot = "PlayerSettingsSave";
	//enable mouse
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;
}


void ALobbyPlayerController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	//Cast<UStealthMultiplayerGameInstance>(GetGameInstance())->DestroySessionAndLeaveGame();
}

void ALobbyPlayerController::InitialSetup_Implementation()
{
	//TODO savegamecheck
	UStealthMultiplayerGameInstance* giRef = Cast<UStealthMultiplayerGameInstance>(GetGameInstance());
	if (giRef->SaveGameCheck(playerSettingsSaveSlot))
	{
		FPlayerInfo loadedInfo = giRef->LoadGame(playerSettingsSaveSlot);
		playerSettings.playerIcon = loadedInfo.playerIcon;
		playerSettings.playerName = loadedInfo.playerName;
		playerSettings.playerCharacter = AStealthMultiplayerCharacter::StaticClass();//Cast<ALobbyGameMode>(UGameplayStatics::GetGameMode(GetWorld()))->DefaultPawnClass;
		playerSettings.isReady = false;
		UE_LOG(LobbyLog, Warning, TEXT("initialsetup - playername: %s"), *playerSettings.playerName);
		giRef->SaveGame(playerSettingsSaveSlot, playerSettings);
	}
	else
	{
		giRef->SaveGame(playerSettingsSaveSlot, playerSettings);
	}

	//if (/*Role == ROLE_Authority*/HasAuthority())
	//{
	//	//CallUpdate(playerSettings, false);
	//	ServerCallUpdate(playerSettings, false);
	//}
	//else
	//{
	//	//ServerCallUpdate(playerSettings, false);
	//	UE_LOG(LobbyLog, Warning, TEXT("initialSetup - no authority"));
	//}
	ServerCallUpdate(playerSettings, false);
}


bool ALobbyPlayerController::InitialSetup_Validate()
{
	return true;
}


void ALobbyPlayerController::LobbySetup_Implementation()
{
	//TODO
	//should be same as showlobby at gameinstance -> still needed?, is called at post login of lobbygamemode
	//showmousecursor
	//create lobby widget and set lobbyRef
	UStealthMultiplayerGameInstance* giRef = Cast<UStealthMultiplayerGameInstance>(GetGameInstance());
	if (!IsValid(lobbyRef))
	{
		lobbyRef = CreateWidget<UUserWidget>(GetWorld(), wLobby);
	}
	//add widget to viewport
	lobbyRef->AddToViewport();
	//EnableMouse();
}


bool ALobbyPlayerController::LobbySetup_Validate()
{
	return true;
}


void ALobbyPlayerController::AddPlayerInfo_Implementation(const TArray<FPlayerInfo>& connectedPlayersInfo)
{
	connectedPlayers = connectedPlayersInfo;
	ULobbyUserWidget* lobbyWidget = Cast<ULobbyUserWidget>(lobbyRef);
	UE_LOG(LobbyLog, Warning, TEXT("AddPlayerInfo - connectedPlayers: %d, validWidget: %s"), connectedPlayers.Num(), IsValid(lobbyWidget) ? TEXT("true") : TEXT("false"));
	if (IsValid(lobbyWidget))
	{
		lobbyWidget->ClearPlayerList();
		for (int i = 0; i < connectedPlayers.Num(); i++)
		{
			lobbyWidget->UpdatePlayerWindow(connectedPlayers[i]);
		}
	}
}


bool ALobbyPlayerController::AddPlayerInfo_Validate(const TArray<FPlayerInfo>& connectedPlayersInfo)
{
	return true;
}


void ALobbyPlayerController::UpdateLobbySettings_Implementation(UTexture2D* mapImage, const FText& mapName)
{
	Cast<ULobbyUserWidget>(lobbyRef)->mapName = mapName;
	Cast<ULobbyUserWidget>(lobbyRef)->mapImage = mapImage;
}


bool ALobbyPlayerController::UpdateLobbySettings_Validate(UTexture2D* mapImage, const FText& mapName)
{
	return true;
}


void ALobbyPlayerController::ClientKicked_Implementation()
{
	//Cast<UStealthMultiplayerGameInstance>(GetGameInstance())->DestroySessionAndLeaveGame();
}


bool ALobbyPlayerController::ClientKicked_Validate()
{
	return true;
}


//void ALobbyPlayerController::CallUpdate(FPlayerInfo playerInfo, bool changedStatus)
//{
//	playerSettings = playerInfo;
//	ALobbyGameMode* gm = Cast<ALobbyGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
//	gm->SwapCharacter(this, playerSettings.playerCharacter, changedStatus);
//	gm->ServerUpdateAll();
//}

void ALobbyPlayerController::ClientUpdateCharacterAvailability_Implementation(const TArray<bool>& availableCharacters)
{
	this->availableCharacters = availableCharacters;
	Cast<ULobbyUserWidget>(lobbyRef)->OnUpdateAvailability();
	//TODO
	//enable and disable buttons depending on selected or not
	//chapter 21
}


void ALobbyPlayerController::ServerCallUpdate_Implementation(FPlayerInfo playerInfo, bool changedStatus)
{
	//CallUpdate(playerInfo, changedStatus);
	playerSettings = playerInfo;
	ALobbyGameMode* gm = Cast<ALobbyGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	gm->SwapCharacter(this, playerSettings.playerCharacter, changedStatus);
	gm->ServerUpdateAll();
}


bool ALobbyPlayerController::ServerCallUpdate_Validate(FPlayerInfo playerInfo, bool changedStatus)
{
	return true;
}


void ALobbyPlayerController::AssignPlayerCharacter_Implementation(UClass* chara, UTexture2D* characterImage, int characterIndex)
{
	TArray<bool> chars = Cast<ALobbyGameMode>(UGameplayStatics::GetGameMode(GetWorld()))->characterAvailability;
	UE_LOG(LobbyLog, Warning, TEXT("assignplayercharacter index: %d, availableChars: %d"), characterIndex, chars.Num());
	previousCharacterIndex = selectedCharacterIndex;
	selectedCharacterIndex = characterIndex;
	if (characterIndex != -1) {
		if (chars[characterIndex]) chars[characterIndex] = false;
		else previousCharacterIndex = -1;
	}
	if(previousCharacterIndex != -1) chars[previousCharacterIndex] = true;
	//assignplayer
	playerSettings.playerCharacter = chara;
	playerSettings.playerCharacterImage = characterImage;
	Cast<UStealthMultiplayerGameInstance>(GetGameInstance())->SaveGame(playerSettingsSaveSlot, playerSettings);
	Cast<ALobbyGameMode>(UGameplayStatics::GetGameMode(GetWorld()))->characterAvailability = chars;
	ServerCallUpdate(playerSettings, false);
}


bool ALobbyPlayerController::AssignPlayerCharacter_Validate(UClass* chara, UTexture2D* characterImage, int characterIndex)
{
	return true;
}


void ALobbyPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to every client, no special condition required
	DOREPLIFETIME(ALobbyPlayerController, playerSettings);
	DOREPLIFETIME(ALobbyPlayerController, connectedPlayers);
	DOREPLIFETIME(ALobbyPlayerController, availableCharacters);
}
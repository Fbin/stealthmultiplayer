// Fill out your copyright notice in the Description page of Project Settings.

#include "HearCharacter.h"
#include "Materials/MaterialParameterCollectionInstance.h"
#include "StealthMultiplayerProjectile.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/SplineMeshActor.h"
#include "Camera/CameraComponent.h"
#include "Components/SplineComponent.h"
#include "Components/SplineMeshComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

AHearCharacter::AHearCharacter()
{
	isDefaultClass = false;

	spline = CreateDefaultSubobject<USplineComponent>(TEXT("SplineComponent"));
	spline->SetupAttachment(GetFirstPersonCameraComponent());
}

void AHearCharacter::PrepareResetActiveAction1()
{
	if (drawAimLine) {
		ServerShootProjectile();
		Super::PrepareResetActiveAction1();

		for (int i = 0; i < splineMeshes.Num(); i++)
		{
			splineMeshes[i]->DestroyComponent();
		}
		splineMeshes.Empty();
		drawAimLine = false;
	}
}

void AHearCharacter::SetupPlayerInputComponent(UInputComponent * InputComponent)
{
	check(InputComponent);

	Super::SetupPlayerInputComponent(InputComponent);

	InputComponent->BindAction("ActiveAction1", IE_Released, this, &AHearCharacter::PrepareResetActiveAction1);
}

void AHearCharacter::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	if (drawAimLine) {
		FPredictProjectilePathParams projectilePathParams;
		projectilePathParams.StartLocation = GetFP_MuzzleLocation()->GetComponentLocation();
		projectilePathParams.LaunchVelocity = GetFirstPersonCameraComponent()->GetForwardVector() * initialProjectileSpeed;
		projectilePathParams.bTraceWithCollision = true;
		projectilePathParams.ObjectTypes = objectTypesToTrace;
		projectilePathParams.ActorsToIgnore = actorsToIgnore;
		projectilePathParams.MaxSimTime = 10.0f;
		//projectilePathParams.DrawDebugType = EDrawDebugTrace::Persistent;
		FPredictProjectilePathResult result;
		UGameplayStatics::PredictProjectilePath(GetWorld(), projectilePathParams, result);

		//splinePoints = result.PathData;
		TArray<FVector> pathVectors;
		for (int i = 0; i < result.PathData.Num(); i++)
		{
			pathVectors.Add(result.PathData[i].Location);
		}
		spline->SetSplinePoints(pathVectors, ESplineCoordinateSpace::World);

		if (result.HitResult.bBlockingHit) {
			FVector startPos, startTangent, endPos, endTangent;
			int lastSplinePoint = spline->GetNumberOfSplinePoints();
			//first element
			if (splineMeshes.Num() == 0) {
				USplineMeshComponent* newSplineMesh = NewObject<USplineMeshComponent>(this);
				newSplineMesh->Mobility = EComponentMobility::Movable;
				newSplineMesh->SetStaticMesh(staticSplineMesh);
				newSplineMesh->SetStartScale(FVector2D(0.1f, 0.0f));
				newSplineMesh->SetEndScale(FVector2D(0.1f, 0.0f));
				newSplineMesh->SetMaterial(0, splineMeshMaterial);
				newSplineMesh->SetupAttachment(GetFirstPersonCameraComponent());
				newSplineMesh->RegisterComponent();
				splineMeshes.Add(newSplineMesh);
			}
			for (int i = 1; i < lastSplinePoint; i++)
			{
				if (splineMeshes.Num() <= i) {
					USplineMeshComponent* newSplineMesh = NewObject<USplineMeshComponent>(this);
					newSplineMesh->Mobility = EComponentMobility::Movable;
					newSplineMesh->SetStaticMesh(staticSplineMesh);
					newSplineMesh->SetStartScale(FVector2D(0.1f, 0.0f));
					newSplineMesh->SetEndScale(FVector2D(0.1f, 0.0f));
					newSplineMesh->SetMaterial(0, splineMeshMaterial);
					newSplineMesh->SetupAttachment(GetFirstPersonCameraComponent());
					newSplineMesh->RegisterComponent();
					splineMeshes.Add(newSplineMesh);
				}
				//update previous splinemesh with SetStartAndEnd
				spline->GetLocationAndTangentAtSplinePoint(i - 1, startPos, startTangent, ESplineCoordinateSpace::Local);
				spline->GetLocationAndTangentAtSplinePoint(i, endPos, endTangent, ESplineCoordinateSpace::Local);
				splineMeshes[i - 1]->SetStartAndEnd(startPos, startTangent, endPos, endTangent);
			}
			//update last splinemesh
			spline->GetLocationAndTangentAtSplinePoint(lastSplinePoint - 1, startPos, startTangent, ESplineCoordinateSpace::Local);
			spline->GetLocationAndTangentAtSplinePoint(lastSplinePoint, endPos, endTangent, ESplineCoordinateSpace::Local);
			splineMeshes[lastSplinePoint - 1]->SetStartAndEnd(startPos, startTangent, endPos, endTangent);

			//delete meshes which are too much
			for (int i = 0; i < splineMeshes.Num(); i++)
			{
				if (i >= lastSplinePoint) {
					splineMeshes[i]->DestroyComponent();
				}
			}
		}
		else {
			for (int i = 0; i < splineMeshes.Num(); i++)
			{
				splineMeshes[i]->DestroyComponent();
			}
			splineMeshes.Empty();
		}
	}

	if (sonarIsActive) {
		currentOpacity -= deltaTime * fadeModifier;
		currentOpacity = FMath::Clamp(currentOpacity, 0.0f, 1.0f);
	}
	else {
		currentOpacity += deltaTime * fadeModifier;
		currentOpacity = FMath::Clamp(currentOpacity, 0.0f, 1.0f);
	}
	if (materialParameterCollection != nullptr) materialParameterCollection->SetScalarParameterValue(FName("OpacityHear"), currentOpacity);
}

void AHearCharacter::ActiveAction1()
{
	if (canUseActiveAction1) {
		Super::ActiveAction1();
		drawAimLine = true;
	}
}

void AHearCharacter::ResetActiveAction1()
{
	Super::ResetActiveAction1();
}

void AHearCharacter::ActiveAction2()
{
	if (canUseActiveAction2) {
		Super::ActiveAction2();
		sonarIsActive = true;
		//TODO set alpha seethrough
		/*if (materialParameterCollection != nullptr) materialParameterCollection->SetScalarParameterValue(FName("OpacityHear"), 0.0f);*/
		//PrepareResetActiveAction2();
		GetWorldTimerManager().SetTimer(resetActiveAction2Handle, this, &AHearCharacter::PrepareResetActiveAction2, activeAction2ActiveTime, false);
	}
}

void AHearCharacter::ResetActiveAction2()
{
	Super::ResetActiveAction2();
	sonarIsActive = false;
	//TODO reset alpha
	//if (materialParameterCollection != nullptr) materialParameterCollection->SetScalarParameterValue(FName("OpacityHear"), 1.0f);
}

void AHearCharacter::ServerShootProjectile_Implementation()
{
	if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{

			const FRotator SpawnRotation = GetControlRotation();
			//TODO maybe change to different offset
			const FVector SpawnLocation = GetActorLocation() + SpawnRotation.RotateVector(GunOffset);

			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

			AStealthMultiplayerProjectile* projectile = World->SpawnActor<AStealthMultiplayerProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
			projectile->GetProjectileMovement()->MaxSpeed = initialProjectileSpeed;
			projectile->GetCollisionComp()->AddImpulse(GetFirstPersonCameraComponent()->GetForwardVector() * initialProjectileSpeed, NAME_None, true);
		}
	}
}

bool AHearCharacter::ServerShootProjectile_Validate()
{
	return true;
}
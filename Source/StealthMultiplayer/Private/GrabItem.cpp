// Fill out your copyright notice in the Description page of Project Settings.

#include "GrabItem.h"
#include "StealthMultiplayerCharacter.h"
#include "UnrealNetwork.h"
#include "CollisionQueryParams.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values
AGrabItem::AGrabItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootSceneComponent"));
	visualComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("staticMesh"));
	visualComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	visualComponent->SetupAttachment(RootComponent);

	colliderComponentRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Collider Root"));
	//colliderComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	//colliderComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	//colliderComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	////TODO change to Interactable channel
	//colliderComponent->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	colliderComponentRoot->SetupAttachment(RootComponent);
	//colliderComponentRoot->OnComponentBeginOverlap.AddDynamic(this, &ATreasure::OnOverlapBegin);

	SetReplicates(true);
	SetReplicateMovement(true);
}

// Called when the game starts or when spawned
void AGrabItem::BeginPlay()
{
	Super::BeginPlay();
	
	if (!allowRotation) {
		initRotation = GetActorRotation();
	}
}

// Called every frame
void AGrabItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGrabItem::Grab_Implementation(USceneComponent * attachTo)
{
	AStealthMultiplayerCharacter* player = Cast<AStealthMultiplayerCharacter>(grabbedBy);
	if (player) {
		SetDirections();
		player->SetGrabbedObject(this);
		player->ServerUpdateCanTurnBody(allowRotation);
		AttachToComponent(attachTo, FAttachmentTransformRules::KeepWorldTransform);
	}
	/*PlayEffects();
	if (HasAuthority()) AttachToComponent(attachTo, FAttachmentTransformRules::KeepWorldTransform);*/
	//set isGrabbing value of player -> needs adjustment of MoveForward() and MoveRight() which axes are still allowed to move along
}

void AGrabItem::Release_Implementation()
{
	AStealthMultiplayerCharacter* player = Cast<AStealthMultiplayerCharacter>(grabbedBy);
	if (player) {
		DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		if(!allowRotation) SetActorRotation(initRotation);
		else SetActorRotation(FRotator(initRotation.Pitch, GetActorRotation().Yaw, initRotation.Roll));
		player->SetGrabbedObject(nullptr);
		player->ServerUpdateCanTurnBody(true);
	}
}

void AGrabItem::Interact_Implementation(AActor * user)
{
	if (grabbedBy == nullptr) {
		grabbedBy = user;
		Execute_Grab(this, Cast<AStealthMultiplayerCharacter>(user)->GetFP_MuzzleLocation());
	}
	else {
		Execute_Release(this);
		grabbedBy = nullptr;
		forwardDirection = FVector::ZeroVector;
		rightDirection = FVector::ZeroVector;
	}
}

void AGrabItem::SetDirections()
{
	const FVector TraceStart = grabbedBy->GetActorLocation();
	const FVector TraceEnd = GetActorLocation();

	FCollisionQueryParams TraceParams(FName(TEXT("TraceDirections")), true, NULL);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = false;
	TraceParams.bTraceComplex = true;
	TraceParams.AddIgnoredActor(grabbedBy);

	FHitResult Hit(ForceInit);
	//TODO change trace chanel for only interactable objects (allows to discern in object to get only components)
	GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceEnd, ECC_Visibility, TraceParams);

	UPrimitiveComponent* v = Hit.GetComponent();
	int index = grabColliderRefs.Find(Hit.GetComponent());
	if (index != INDEX_NONE) {
		if (allowMoveForward.Num() - 1 < index || allowMoveRight.Num() - 1 < index) {
			UE_LOG(LogTemp, Warning, TEXT("allowMove arrays not big enough, forward missing: %d, right missing: %d"), allowMoveForward.Num() - index - 1, allowMoveRight.Num() - index - 1);
		}
		else {
			if (allowMoveForward[index]) forwardDirection = -Hit.ImpactNormal;
			else forwardDirection = FVector::ZeroVector;
			if (allowMoveRight[index]) rightDirection = Hit.ImpactNormal.RotateAngleAxis(-90.0f, FVector::UpVector);
			else rightDirection = FVector::ZeroVector;
		}
	}
	else UE_LOG(LogTemp, Warning, TEXT("no references in 'grabColliderRef' for '%s' found"), *UKismetSystemLibrary::GetDisplayName(Hit.GetComponent()));
}

FVector AGrabItem::GetMovementForwardVector()
{
	return forwardDirection;
}

FVector AGrabItem::GetMovementRightVector()
{
	return rightDirection;
}

void AGrabItem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AGrabItem, grabbedBy);
	DOREPLIFETIME(AGrabItem, forwardDirection);
	DOREPLIFETIME(AGrabItem, rightDirection);
}
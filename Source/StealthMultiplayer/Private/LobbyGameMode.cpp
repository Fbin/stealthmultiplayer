// Fill out your copyright notice in the Description page of Project Settings.

#include "LobbyGameMode.h"
#include "StealthMultiplayer.h"
#include "StealthMultiplayerPlyrController.h"
#include "StealthMultiplayerGameInstance.h"
//#include "MultiplayerCharacter.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"


ALobbyGameMode::ALobbyGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	/* Assign the class types used by this gamemode */
	PlayerControllerClass = ALobbyPlayerController::StaticClass();
	//PlayerStateClass = ASPlayerState::StaticClass();
	//GameStateClass = ASGameState::StaticClass();
	//SpectatorClass = ASSpectatorPawn::StaticClass();
	//TODO
	//has to be true, else servertravel wont work for launching the game
	bActorSeamlessTraveled = true;
}

void ALobbyGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	if (HasAuthority())
	{
		UStealthMultiplayerGameInstance* gIRef = Cast<UStealthMultiplayerGameInstance>(GetGameInstance());
		/*gIRef->*/allPlayerController.Add(NewPlayer);
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), startPointClass, startPoints);
		ALobbyPlayerController* pCRef = Cast<ALobbyPlayerController>(NewPlayer);
		//TODO
		//allways breaks here (was because of no default values, are now set)
		pCRef->LobbySetup();
		pCRef->InitialSetup();
		pCRef->UpdateLobbySettings(gMMapImage, gMMapName);
		//TODO
		//maybe move spawn to gamestart not here or make him just rotate for preview
		//respawn too early, initialsetup not yet completed so new player in players is without information
		RespawnPlayer(NewPlayer);
		for (int i = 0; i < players.Num(); i++)
		{
			UE_LOG(LobbyLog, Warning, TEXT("postlogin - playername: %s"), *players[i].playerName);
		}
	}
	else
	{
		//should never happen, because gm only exists on server -> only server has authority
	}
}

void ALobbyGameMode::Logout(AController * Exiting)
{
	Super::Logout(Exiting);

	//TArray<APlayerController*> allPlayerController = Cast<UStealthMultiplayerGameInstance>(GetGameInstance())->allPlayerController;
	int indexToRemove = -1;
	for (int i = 0; i < allPlayerController.Num(); i++)
	{
		if (Cast<ALobbyPlayerController>(Exiting) == allPlayerController[i]) {
			indexToRemove = i;
			break;
		}
	}
	int selectedIndex = Cast<ALobbyPlayerController>(Exiting)->selectedCharacterIndex;
	if (selectedIndex != -1) {
		characterAvailability[selectedIndex] = true;
	}

	if (indexToRemove != -1) {
		//TODO check if this is the correct way to remove
		allPlayerController.RemoveAt(indexToRemove);
		players.RemoveAt(indexToRemove);
		UpdateAll();
	}
}

void ALobbyGameMode::SetPlayerCharacter(UClass* selectedCharacter, int index)
{
	players[index].playerCharacter = selectedCharacter;
}

bool ALobbyGameMode::CheckAllReady()
{
	int notReadyCount = 0;
	for (int i = 0; i < players.Num(); i++) {
		if (!players[i].isReady) {
			if (notReadyCount == 0) notReadyCount++;
			else return false;
		}
	}
	return true;
}

//void ALobbyGameMode::SetPlayerWeapon(AWeapon* selectedWeapon, int index)
//{
//	players[index].standardWeapon = selectedWeapon;
//}
//
//
//void ALobbyGameMode::SetPlayerItem(AItem* selectedItem, int index)
//{
//	players[index].standardItem = selectedItem;
//}


void ALobbyGameMode::SwapCharacter(APlayerController* playerController, TSubclassOf<ACharacter> newCharacter, bool changedStatus)
{
	if (changedStatus)
	{

	}
	else
	{
		if (IsValid(playerController->GetPawn()))
		{
			playerController->GetPawn()->Destroy();
		}

		int rndNr = FMath::RandRange(0, startPoints.Num() - 1);
		FVector StartLocation;
		FRotator StartRotation;
		if (startPoints.Num() != 0)
		{
			StartLocation = startPoints[rndNr]->GetActorLocation();
			StartRotation = startPoints[rndNr]->GetActorRotation();
		}
		else
		{
			StartLocation = FVector(0, 0, 0);
			StartRotation = FRotator(0, 0, 0);
		}
		FActorSpawnParameters p;
		APawn* ResultPawn = GetWorld()->SpawnActor<APawn>(newCharacter, StartLocation, StartRotation, p);
		playerController->Possess(ResultPawn);
	}
}


void ALobbyGameMode::ServerSwapCharacter_Implementation(APlayerController* playerController, TSubclassOf<ACharacter> newCharacter, bool changedStatus)
{
	SwapCharacter(playerController, newCharacter, changedStatus);
}

bool ALobbyGameMode::ServerSwapCharacter_Validate(APlayerController* playerController, TSubclassOf<ACharacter> newCharacter, bool changedStatus)
{
	return true;
}


void ALobbyGameMode::UpdateAll()
{
	UStealthMultiplayerGameInstance* gIRef = Cast<UStealthMultiplayerGameInstance>(GetGameInstance());
	currentPlayerAmount = /*gIRef->*/allPlayerController.Num();
	if (currentPlayerAmount > 0)
	{
		players.Empty();
		for (int i = 0; i < currentPlayerAmount; i++)
		{
			players.Add(Cast<ALobbyPlayerController>(/*gIRef->*/allPlayerController[i])->playerSettings);
		}
		for (int i = 0; i < currentPlayerAmount; i++)
		{
			ALobbyPlayerController* pcRef = Cast<ALobbyPlayerController>(/*gIRef->*/allPlayerController[i]);
			pcRef->AddPlayerInfo(players);
			AddToKickList();
			pcRef->ClientUpdateCharacterAvailability(characterAvailability);
			UE_LOG(LobbyLog, Warning, TEXT("UpdateAll() - name: %s, charsAmount: %d"), *pcRef->playerSettings.playerName, characterAvailability.Num());
		}
		for (int i = 0; i < players.Num(); i++)
		{
			if (players[i].playerCharacter == DefaultPawnClass)
			{
				CanStart = false;
				break;
			}
			else
			{
				CanStart = true;
			}
		}
	}
}


void ALobbyGameMode::ServerUpdateAll_Implementation()
{
	UpdateAll();
}

bool ALobbyGameMode::ServerUpdateAll_Validate()
{
	return true;
}


void ALobbyGameMode::ServerUpdateGameSettings_Implementation(UTexture2D* mapImage, const FText& mapName, int mapId)
{
	gMMapImage = mapImage;
	gMMapName = mapName;
	gMMapId = mapId;
	//UStealthMultiplayerGameInstance* gI = Cast<UStealthMultiplayerGameInstance>(GetGameInstance());
	for (int i = 0; i < /*gI->*/allPlayerController.Num(); i++)
	{
		Cast<ALobbyPlayerController>(/*gI->*/allPlayerController[i])->UpdateLobbySettings(gMMapImage, gMMapName);
	}
}

bool ALobbyGameMode::ServerUpdateGameSettings_Validate(UTexture2D* mapImage, const FText& mapName, int mapId)
{
	return true;
}


void ALobbyGameMode::RespawnPlayer_Implementation(APlayerController* pcRef)
{
	if (IsValid(pcRef->GetPawn()))
	{
		pcRef->GetPawn()->Destroy();
	}
	int rndNr = FMath::RandRange(0, startPoints.Num() - 1);
	FVector StartLocation;
	FRotator StartRotation;
	if (startPoints.Num() != 0)
	{
		StartLocation = startPoints[rndNr]->GetActorLocation();
		StartRotation = startPoints[rndNr]->GetActorRotation();
	}
	else
	{
		StartLocation = FVector(0, 0, 0);
		StartRotation = FRotator(0, 0, 0);
	}
	FActorSpawnParameters p;
	APawn* ResultPawn = GetWorld()->SpawnActor<APawn>(DefaultPawnClass, StartLocation, StartRotation, p);
	pcRef->Possess(ResultPawn);
	
	//TODO do i have to check for authority?
	/*if (HasAuthority())
	{*/
		//UpdateAll();
	/*}
	else
	{*/
		ServerUpdateAll();
	//}
	/*ALobbyPlayerController* controller = Cast<ALobbyPlayerController>(pcRef);
	if(controller != nullptr) controller->ClientUpdateCharacterAvailability(characterAvailability);*/
}

bool ALobbyGameMode::RespawnPlayer_Validate(APlayerController* pcRef)
{
	return true;
}


void ALobbyGameMode::AddToKickList()
{
	//TODO
	//call to function of gamesettings widget if the widget is up to update the data => updateWindow()
	//ref to widget is in StealthMultiplayerGameInstance
	//make sure the ref is valid before calling to it
}

void ALobbyGameMode::ServerKickPlayer_Implementation(int playerId)
{
	//Cast<UStealthMultiplayerGameInstance>(GetGameInstance())->allPlayerController[playerId]->ClientWasKicked();
}

bool ALobbyGameMode::ServerKickPlayer_Validate(int playerId)
{
	return true;
}


void ALobbyGameMode::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to every client, no special condition required
	DOREPLIFETIME(ALobbyGameMode, players);
	DOREPLIFETIME(ALobbyGameMode, currentPlayerAmount);
	DOREPLIFETIME(ALobbyGameMode, startPoints);
	DOREPLIFETIME(ALobbyGameMode, gMMapImage);
	DOREPLIFETIME(ALobbyGameMode, gMMapName);
	DOREPLIFETIME(ALobbyGameMode, gMMapId);
	DOREPLIFETIME(ALobbyGameMode, allPlayerController);
}
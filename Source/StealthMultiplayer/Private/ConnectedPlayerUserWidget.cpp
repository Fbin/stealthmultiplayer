// Fill out your copyright notice in the Description page of Project Settings.

#include "ConnectedPlayerUserWidget.h"


void UConnectedPlayerUserWidget::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to every client, no special condition required
	DOREPLIFETIME(UConnectedPlayerUserWidget, playerSettings);
	DOREPLIFETIME(UConnectedPlayerUserWidget, connectedPlayerName);
	DOREPLIFETIME(UConnectedPlayerUserWidget, selectedPlayerCharacter);
	DOREPLIFETIME(UConnectedPlayerUserWidget, readyStatus);
}
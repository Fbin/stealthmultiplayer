// Fill out your copyright notice in the Description page of Project Settings.

#include "StealthMultiplayerSaveGame.h"
#include "StealthMultiplayer.h"

void UStealthMultiplayerSaveGame::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to every client, no special condition required
	DOREPLIFETIME(UStealthMultiplayerSaveGame, S_PlayerInfo);
}
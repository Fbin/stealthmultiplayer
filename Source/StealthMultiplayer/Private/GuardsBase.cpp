// Fill out your copyright notice in the Description page of Project Settings.

#include "GuardsBase.h"
#include "Perception/PawnSensingComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Components/WidgetComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Net/UnrealNetwork.h"
#include "Types.h"
#include "AIController.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "NavigationSystem.h"

// Sets default values
AGuardsBase::AGuardsBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	widgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("WidgetComponent"));
	widgetComponent->SetupAttachment(RootComponent);
	widgetComponent->SetDrawAtDesiredSize(true);
	widgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
	
	pawnSensingComponent = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComponent"));
	pawnSensingComponent->OnSeePawn.AddDynamic(this, &AGuardsBase::OnSeePawn);
	pawnSensingComponent->OnHearNoise.AddDynamic(this, &AGuardsBase::OnNoiseHeard);

	currentPatrolPoint = 0;
	allowedDistanceToPatrolPoints = 100.0f;
	guardState = EGuardState::GuardState_Idle;
	bUseControllerRotationYaw = false;
	GetCharacterMovement()->bOrientRotationToMovement = true;

	SetReplicates(true);
	SetReplicateMovement(true);
}

// Called when the game starts or when spawned
void AGuardsBase::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority()) {
		if (patrolPoints.Num() == 0) UE_LOG(LogTemp, Error, TEXT("no patrolPoints defined"));
		
		sightDistanceDefault = pawnSensingComponent->SightRadius;
		hearingDistanceDefault = pawnSensingComponent->HearingThreshold;
		losHearingDistanceDefault = pawnSensingComponent->LOSHearingThreshold;

		aiControllerRef = UAIBlueprintHelperLibrary::GetAIController(this);
		targetLocation = GetActorLocation();
		targetRotation = GetActorRotation();
	}
}

void AGuardsBase::OnSeePawn(APawn* seenPawn)
{
	if(GetWorldTimerManager().IsTimerActive(timerHandle_ResetOrientation)) GetWorldTimerManager().ClearTimer(timerHandle_ResetOrientation);
	float dist = FVector::Dist(GetActorLocation(), seenPawn->GetActorLocation());

	if (dist >= minSeeDistanceToSuspicious) SeeReaction(seenPawn, EGuardState::GuardState_Suspicious);
	else if (dist >= minSeeDistanceToSearching) SeeReaction(seenPawn, EGuardState::GuardState_Searching);
	else SeeReaction(seenPawn, EGuardState::GuardState_Alert);
}

void AGuardsBase::SeeReaction(APawn* seenPawn, EGuardState newState)
{
	switch (newState)
	{
	case EGuardState::GuardState_Idle:
		break;
	case EGuardState::GuardState_Suspicious:
		GetController()->StopMovement();
		targetRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), seenPawn->GetActorLocation());
		break;
	case EGuardState::GuardState_Alert:
		GetController()->StopMovement();
		targetRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), seenPawn->GetActorLocation());
		GetWorldTimerManager().ClearTimer(timerHandle_ResetOrientation);
		GetWorldTimerManager().SetTimer(timerHandle_ResetOrientation, this, &AGuardsBase::ResetOrientation, resetTime);
		//TODO
		//set player to gameover
		//maybe instead of resetTimer and going back to duty -> dont turn player into game over and let guard place him instead into cell
		break;
	case EGuardState::GuardState_Searching:
		searchCounter = 0;
		targetLocation = seenPawn->GetActorLocation();
		aiControllerRef->MoveToLocation(targetLocation, 1.0f, false, true, true, true, nullptr, true);
		break;
	default:
		break;
	}
	SetGuardState(newState);
}

void AGuardsBase::OnNoiseHeard(APawn * heardPawn, const FVector & location, float volume)
{
	switch (guardState)
	{
	case EGuardState::GuardState_Idle:
		NoiseReaction(location, EGuardState::GuardState_Suspicious);
		break;
	case EGuardState::GuardState_Suspicious:
		NoiseReaction(location, EGuardState::GuardState_Searching);
		break;
	case EGuardState::GuardState_Alert:
		//Gameover
		break;
	case EGuardState::GuardState_Searching:
		NoiseReaction(location, EGuardState::GuardState_Searching);
		break;
	default:
		break;
	}
}

void AGuardsBase::NoiseReaction(const FVector & location, EGuardState newState)
{
	if (GetWorldTimerManager().IsTimerActive(timerHandle_ResetOrientation)) GetWorldTimerManager().ClearTimer(timerHandle_ResetOrientation);

	GetController()->StopMovement();
	FVector dir = location - GetActorLocation();
	dir.Normalize();
	FRotator newRotation = FRotationMatrix::MakeFromX(dir).Rotator();
	newRotation.Pitch = 0.0f;
	newRotation.Roll = 0.0f;

	if (guardType == EGuardType::Stationary) {
		//TODO change from instant to lerp rotation, should be but needs testing
		//skeletalComponent->SetWorldRotation(newRotation);
		targetRotation = newRotation;
	}
	else {
		if (newState == EGuardState::GuardState_Suspicious) {
			//TODO change from instant to lerp rotation, should be but needs testing
			//skeletalComponent->SetWorldRotation(newRotation);
			targetRotation = newRotation;
		}
		else {
			//only entered if newState == EGuardState::GuardState_Searching
			searchCounter = 0;
			targetLocation = location;
			aiControllerRef->MoveToLocation(targetLocation, 1.0f, false, true, true, true, nullptr, true);
		}
	}

	SetGuardState(newState);
}

void AGuardsBase::ResetOrientation()
{
	searchCounter = 0;
	targetLocation = patrolPoints[currentPatrolPoint].location;
	targetRotation = patrolPoints[currentPatrolPoint].rotation;
	aiControllerRef->MoveToLocation(targetLocation, 1.0f, false, true, true, true, nullptr, true);

	SetGuardState(EGuardState::GuardState_Idle);
}

void AGuardsBase::OnRep_GuardState()
{
	OnUpdateGuardState(guardState);
}

void AGuardsBase::SetGuardState(EGuardState newState)
{
	if (guardState == newState) return;

	switch (newState)
	{
	case EGuardState::GuardState_Idle:
		pawnSensingComponent->SightRadius = sightDistanceDefault;
		minSeeDistanceToSuspicious = minSeeDistanceToSuspiciousDefault;
		minSeeDistanceToSearching = minSeeDistanceToSearchingDefault;
		pawnSensingComponent->HearingThreshold = hearingDistanceDefault;
		pawnSensingComponent->LOSHearingThreshold = losHearingDistanceDefault;
		break;
	case EGuardState::GuardState_Suspicious:
		pawnSensingComponent->SightRadius = sightDistanceUnderSuspicion;
		minSeeDistanceToSuspicious = minSeeDistanceToSuspiciousUnderSuspicion;
		minSeeDistanceToSearching = minSeeDistanceToSearchingUnderSuspicion;
		pawnSensingComponent->HearingThreshold = hearingDistanceUnderSuspicion;
		pawnSensingComponent->LOSHearingThreshold = losHearingDistanceUnderSuspicion;
		break;
	case EGuardState::GuardState_Alert:
		break;
	case EGuardState::GuardState_Searching:
		pawnSensingComponent->SightRadius = sightDistanceUnderSearching;
		minSeeDistanceToSuspicious = sightDistanceUnderSearching;
		minSeeDistanceToSearching = minSeeDistanceToSearchingUnderSearching;
		pawnSensingComponent->HearingThreshold = hearingDistanceUnderSearching;
		pawnSensingComponent->LOSHearingThreshold = losHearingDistanceUnderSearching;
		break;
	default:
		break;
	}

	guardState = newState;
	OnRep_GuardState();
}

void AGuardsBase::Patrol()
{
	FVector direction;
	if (patrolPoints.Num() > 0) {
		rotationTime = 0.0f;
		switch (guardType)
		{
		case EGuardType::Stationary:
			currentPatrolPoint++;
			currentPatrolPoint %= patrolPoints.Num();
			break;
		case EGuardType::PatrolLinear:
			if (patrolUp) {
				if (currentPatrolPoint + 1 < patrolPoints.Num()) currentPatrolPoint++;
				else {
					patrolUp = false;
					currentPatrolPoint--;
				}
			}
			else {
				if (currentPatrolPoint > 0) currentPatrolPoint--;
				else {
					patrolUp = true;
					currentPatrolPoint++;
				}
			}
			break;
		case EGuardType::PatrolCircular:
			currentPatrolPoint++;
			currentPatrolPoint %= patrolPoints.Num();
			break;
		case EGuardType::Roaming: {
			UWorld* World = GetWorld();
			UNavigationSystemV1* NavSys = FNavigationSystem::GetCurrent<UNavigationSystemV1>(World);
			if (NavSys) {
				FVector origin = patrolPoints[0].location;
				FNavLocation RandomPoint(origin);
				NavSys->GetRandomReachablePointInRadius(origin, roamingRadius, RandomPoint);
				targetLocation = RandomPoint.Location;
			}
			break;
		}
		default:
			UE_LOG(LogTemp, Error, TEXT("GuardBase.Patrol called with undefined guardType"));
			break;
		}
		
		if (guardType != EGuardType::Roaming) {
			targetLocation = patrolPoints[currentPatrolPoint].location;
			targetRotation = patrolPoints[currentPatrolPoint].rotation;
		}
		/*EPathFollowingRequestResult::Type result = */aiControllerRef->MoveToLocation(targetLocation, allowedDistanceToPatrolPoints, false, true, true, true, nullptr, true);
		//UEnum* ePtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EPathFollowingRequestResult"), true);
		//UE_LOG(LogTemp, Warning, TEXT("move result: %s, location: %s, controller: %s"), *ePtr->GetNameByValue(result).ToString(), *targetLocation.ToString(), *aiControllerRef->GetName());
	}
}

bool AGuardsBase::ReachedDestination()
{
	return GetVelocity().Size() == 0.0f && (targetLocation - GetActorLocation()).Size() <= allowedDistanceToPatrolPoints;
}

bool AGuardsBase::ReachedRotation()
{
	switch (guardType)
	{
	case EGuardType::Stationary:
		//TODO maybe use ReachedDesiredRotation() instead
		return targetRotation.Yaw - GetActorRotation().Yaw <= 1.0f;
		break;
	case EGuardType::PatrolLinear:
		return targetRotation.Yaw - GetActorRotation().Yaw <= 1.0f; 
		break;
	case EGuardType::PatrolCircular:
		return targetRotation.Yaw - GetActorRotation().Yaw <= 1.0f; 
		break;
	case EGuardType::Roaming:
		break;
	default:
		break;
	}
	return true;
}

void AGuardsBase::Rotate(float deltaTime)
{
	FRotator newRotation = FMath::RInterpConstantTo(GetActorRotation(), targetRotation, deltaTime, rotationSpeed);
	RootComponent->SetWorldRotation(newRotation);
}

// Called every frame
void AGuardsBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasAuthority()) {
		switch (guardState)
		{
		case EGuardState::GuardState_Idle:
			if (!GetWorldTimerManager().IsTimerActive(timerHandle_PatrolDelay)) {
				if (ReachedDestination()) {
					if (ReachedRotation()) {
						//GetWorldTimerManager().ClearTimer(timerHandle_PatrolDelay);
						GetWorldTimerManager().SetTimer(timerHandle_PatrolDelay, this, &AGuardsBase::Patrol, patrolPoints[currentPatrolPoint].waitingTime);
					}
					else Rotate(DeltaTime);
				}
			}
			break;
		case EGuardState::GuardState_Suspicious:
			if (targetRotation.Yaw - GetActorRotation().Yaw <= 1.0f) {
				GetWorldTimerManager().ClearTimer(timerHandle_ResetOrientation);
				GetWorldTimerManager().SetTimer(timerHandle_ResetOrientation, this, &AGuardsBase::ResetOrientation, resetTime);
			}
			break;
		case EGuardState::GuardState_Alert:
			//TODO walk to target, put him into custody and then go back to idle?
			break;
		case EGuardState::GuardState_Searching:
			//TODO check if it still works in case initial targetlocation is not reachable
			//TODO should this only happen with non-stationary guards or all?
			if (ReachedDestination()) {
				if (searchCounter < maxSearchPoints) {
					if (searchCounter == 0) searchCenter = GetActorLocation();
					searchCounter++;
					UWorld* World = GetWorld();
					UNavigationSystemV1* NavSys = FNavigationSystem::GetCurrent<UNavigationSystemV1>(World);
					if (NavSys) {
						FNavLocation RandomPoint(searchCenter);
						NavSys->GetRandomReachablePointInRadius(searchCenter, searchingRadius, RandomPoint);
						targetLocation = RandomPoint.Location;
					}
					aiControllerRef->MoveToLocation(targetLocation, allowedDistanceToPatrolPoints, false, true, true, true, nullptr, true);
				}
				else {
					GetWorldTimerManager().ClearTimer(timerHandle_ResetOrientation);
					GetWorldTimerManager().SetTimer(timerHandle_ResetOrientation, this, &AGuardsBase::ResetOrientation, resetTime);
				}
			}
			break;
		default:
			break;
		}
	}
}

void AGuardsBase::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AGuardsBase, guardState);
}
// Fill out your copyright notice in the Description page of Project Settings.

#include "GrapplingHook.h"
#include "CableComponent.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "SpeakCharacter.h"
//#include "UnrealNetwork.h"

AGrapplingHook::AGrapplingHook()
{
	PrimaryActorTick.bCanEverTick = true;

	cableComponent = CreateDefaultSubobject<UCableComponent>(TEXT("CableComponent"));
	cableComponent->EndLocation = FVector(0.0f, 0.0f, 0.0f);
	cableComponent->CableLength = 1.0f;
	cableComponent->CableWidth = 5.0f;
	//cableComponent->SetVisibility(false);
	RootComponent = cableComponent;

	boxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	//TODO setup collider defaults
	//TODO set default box width
	boxComponent->SetupAttachment(cableComponent);

	SetReplicates(true);
	SetReplicateMovement(true);
}

void AGrapplingHook::Shoot(bool didHookHit, FVector newtargetLocation, float hookSpeed, float hookLength, AActor* initiator, FName ComponentProperty, FName SocketName/* = NAME_None*/)
{
	hookHitSomething = didHookHit;
	targetLocation = newtargetLocation;
	grapplingHookSpeed = hookSpeed;
	maxCableLength = hookLength;
	otherActor = initiator;
	//cableComponent->SetVisibility(true);
	//cableComponent->SetAttachEndTo(initiator, ComponentProperty, SocketName);
	MulticastAttachGrapplingHook(initiator, ComponentProperty, SocketName);
	tickEnabled = true;
}

void AGrapplingHook::Place()
{
	////TODO adjust boxComponent on local as well
	//float floorOffset = Cast<ASpeakCharacter>(otherActor)->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	//FVector midPoint = (otherActor->GetActorLocation() + GetActorLocation() - FVector(0.0f, 0.0f, floorOffset)) / 2.0f;
	//FVector direction = otherActor->GetActorLocation() - FVector(0.0f, 0.0f, floorOffset) - GetActorLocation();
	//FVector boxLocation = midPoint - GetActorLocation();

	//boxComponent->SetRelativeLocationAndRotation(boxLocation, FRotator(direction.Rotation().Pitch, direction.Rotation().Yaw, 0.0f).Quaternion());
	//boxComponent->SetBoxExtent(FVector(direction.Size() / 2.0f, 20.0f, 5.0f));
	//boxComponent->SetHiddenInGame(false);
	////TODO set collision only for speak character
	//boxComponent->SetCollisionProfileName(TEXT("BlockAllDynamic"));

	//cableComponent->SetAttachEndTo(nullptr, NAME_None, NAME_None);
	////MulticastAttachGrapplingHook(nullptr, NAME_None, NAME_None, direction);
	//SetActorRotation(FRotator(0.0f, 0.0f, 0.0f));
	//cableComponent->EndLocation = direction;
	MulticastPlace(otherActor);

	Cast<ASpeakCharacter>(otherActor)->PrepareResetActiveAction1();
	Cast<ASpeakCharacter>(otherActor)->PrepareResetActiveAction2();
}

void AGrapplingHook::CheckHookLength()
{
	//cable reach max length without reaching the target e.g. player is falling
	float currentCableLength = (GetActorLocation() - otherActor->GetActorLocation()).Size();
	if (currentCableLength >= maxCableLength) {
		Cast<ASpeakCharacter>(otherActor)->PrepareResetActiveAction1();
		Destroy();
	}
	else if ((currentCableLength / cableComponent->NumSegments) > maxSegmentLength) MulticastIncreaseNumSegments();
}

void AGrapplingHook::MulticastPlace_Implementation(AActor* target)
{
	//TODO adjust boxComponent on local as well
	float floorOffset = Cast<ASpeakCharacter>(target)->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	FVector midPoint = (target->GetActorLocation() + GetActorLocation() - FVector(0.0f, 0.0f, floorOffset)) / 2.0f;
	FVector direction = target->GetActorLocation() - FVector(0.0f, 0.0f, floorOffset) - GetActorLocation();
	FVector boxLocation = midPoint - GetActorLocation();

	boxComponent->SetRelativeLocationAndRotation(boxLocation, FRotator(direction.Rotation().Pitch, direction.Rotation().Yaw, 0.0f).Quaternion());
	boxComponent->SetBoxExtent(FVector(direction.Size() / 2.0f, 20.0f, 5.0f));
	boxComponent->SetHiddenInGame(false);
	//TODO set collision only for speak character
	boxComponent->SetCollisionProfileName(TEXT("BlockAllDynamic"));

	cableComponent->SetAttachEndTo(nullptr, NAME_None, NAME_None);
	//MulticastAttachGrapplingHook(nullptr, NAME_None, NAME_None, direction);
	SetActorRotation(FRotator(0.0f, 0.0f, 0.0f));
	cableComponent->EndLocation = direction;
}

bool AGrapplingHook::MulticastPlace_Validate(AActor* target)
{
	return true;
}

void AGrapplingHook::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	if (tickEnabled) {
		float grappleDelay = (GetActorLocation() - targetLocation).Size() / grapplingHookSpeed;
		FVector newLocation = FMath::VInterpTo(GetActorLocation(), targetLocation, deltaTime, 1.0f / grappleDelay);
		SetActorLocation(newLocation);

		if (GetActorLocation().Equals(targetLocation, 0.1f)) {
			if (hookHitSomething) {
				Cast<ASpeakCharacter>(otherActor)->SetGrapplingHookReadyForReelingIn();
				tickEnabled = false;
			}
			else {
				Cast<ASpeakCharacter>(otherActor)->PrepareResetActiveAction1();
				Destroy();
			}
		}

		CheckHookLength();
	}
}

void AGrapplingHook::MulticastAttachGrapplingHook_Implementation(AActor* initiator, FName ComponentProperty, FName SocketName/* = NAME_None*/)
{
	cableComponent->SetAttachEndTo(initiator, ComponentProperty, SocketName);
}

bool AGrapplingHook::MulticastAttachGrapplingHook_Validate(AActor* initiator, FName ComponentProperty, FName SocketName/* = NAME_None*/)
{
	return true;
}

void AGrapplingHook::MulticastIncreaseNumSegments_Implementation()
{
	cableComponent->NumSegments++;
}

bool AGrapplingHook::MulticastIncreaseNumSegments_Validate()
{
	return true;
}

//void AGrapplingHook::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
//{
//	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
//
//	DOREPLIFETIME(AGrapplingHook, maxCableLength);
//	DOREPLIFETIME(AGrapplingHook, otherActor);
//}
// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "StealthMultiplayerCharacter.h"
#include "StealthMultiplayerProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "InteractableInterface.h"
#include "GrabItem.h"
#include "Components/PawnNoiseEmitterComponent.h"
#include "UnrealNetwork.h"
//#include "DrawDebugHelpers.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AStealthMultiplayerCharacter

AStealthMultiplayerCharacter::AStealthMultiplayerCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	//Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	//FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	pawnNoiseEmitterComponent = CreateDefaultSubobject<UPawnNoiseEmitterComponent>(TEXT("NoiseEmitterComponent"));

	SetReplicates(true);
	SetReplicateMovement(true);
}

void AStealthMultiplayerCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	Mesh1P->SetHiddenInGame(false, true);
	if(materialParameterCollectionRef != nullptr) materialParameterCollection = GetWorld()->GetParameterCollectionInstance(materialParameterCollectionRef);
}

void AStealthMultiplayerCharacter::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	if (!IsLocallyControlled()) {
		FRotator newRot = FirstPersonCameraComponent->RelativeRotation;
		newRot.Pitch = RemoteViewPitch * 360.0f / 255.0f; //to have 360 degree instead of uint8 saved value (prevents negative);

		FirstPersonCameraComponent->SetRelativeRotation(newRot);
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AStealthMultiplayerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AStealthMultiplayerCharacter::OnFire);

	PlayerInputComponent->BindAction("ActiveAction1", IE_Pressed, this, &AStealthMultiplayerCharacter::ActiveAction1);
	PlayerInputComponent->BindAction("ActiveAction2", IE_Pressed, this, &AStealthMultiplayerCharacter::ActiveAction2);
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AStealthMultiplayerCharacter::Interact);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AStealthMultiplayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AStealthMultiplayerCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AStealthMultiplayerCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AStealthMultiplayerCharacter::LookUpAtRate);
}

AActor* AStealthMultiplayerCharacter::GetObjectInView()
{
	FVector CamLoc;
	FRotator CamRot;

	if (Controller == NULL)
		return nullptr;

	Controller->GetPlayerViewPoint(CamLoc, CamRot);
	const FVector TraceStart = CamLoc;
	const FVector Direction = CamRot.Vector();
	const FVector TraceEnd = TraceStart + (Direction * maxUseDistance);

	FCollisionQueryParams TraceParams(FName(TEXT("TraceUsableActor")), true, this);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = false;
	TraceParams.bTraceComplex = true;

	FHitResult Hit(ForceInit);
	//TODO change trace chanel for only interactable objects (allows to discern in object to get only components)
	GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceEnd, ECC_Visibility, TraceParams);

	//DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Red, false, 10.0f);

	return Hit.GetActor();
}

void AStealthMultiplayerCharacter::Interact()
{
	//TODO
	if (grabbedObject != nullptr) ServerInteract(grabbedObject);
	else {
		AActor * objectToInteractWith = GetObjectInView();
		if (objectToInteractWith != nullptr) {
			if (objectToInteractWith->GetClass()->ImplementsInterface(UInteractableInterface::StaticClass())) /*IInteractableInterface::Execute_Interact(objectToInteractWith, this);*/ServerInteract(objectToInteractWith);
		}
	}
}

void AStealthMultiplayerCharacter::ServerInteract_Implementation(AActor* target)
{
	IInteractableInterface::Execute_Interact(target, this);
}

bool AStealthMultiplayerCharacter::ServerInteract_Validate(AActor* target)
{
	return true;
}

void AStealthMultiplayerCharacter::ServerActiveAction1_Implementation()
{
	canUseActiveAction1 = false;
	ReceiveActiveAction1();
}

bool AStealthMultiplayerCharacter::ServerActiveAction1_Validate()
{
	return true;
}

void AStealthMultiplayerCharacter::ActiveAction1()
{
	ServerActiveAction1();
}

void AStealthMultiplayerCharacter::ServerResetActiveAction1_Implementation()
{
	canUseActiveAction1 = true;
}

bool AStealthMultiplayerCharacter::ServerResetActiveAction1_Validate()
{
	return true;
}

void AStealthMultiplayerCharacter::ResetActiveAction1()
{
	GetWorldTimerManager().ClearTimer(resetActiveAction1Handle);
	ServerResetActiveAction1();
}

void AStealthMultiplayerCharacter::PrepareResetActiveAction1()
{
	GetWorldTimerManager().SetTimer(resetActiveAction1Handle, this, &AStealthMultiplayerCharacter::ResetActiveAction1, activeAction1ReuseDelay, false);
}

void AStealthMultiplayerCharacter::ServerActiveAction2_Implementation()
{
	canUseActiveAction2 = false;
	ReceiveActiveAction2();
}

bool AStealthMultiplayerCharacter::ServerActiveAction2_Validate()
{
	return true;
}

void AStealthMultiplayerCharacter::ActiveAction2()
{
	ServerActiveAction2();
}

void AStealthMultiplayerCharacter::ServerResetActiveAction2_Implementation()
{
	canUseActiveAction2 = true;
}

bool AStealthMultiplayerCharacter::ServerResetActiveAction2_Validate()
{
	return true;
}

void AStealthMultiplayerCharacter::ResetActiveAction2()
{
	GetWorldTimerManager().ClearTimer(resetActiveAction2Handle);
	ServerResetActiveAction2();
}

void AStealthMultiplayerCharacter::ServerUpdateCanTurnBody_Implementation(bool newState)
{
	canTurnBody = newState;
	MulticastUpdateCanTurnBody(canTurnBody);
}

bool AStealthMultiplayerCharacter::ServerUpdateCanTurnBody_Validate(bool newState)
{
	return true;
}

void AStealthMultiplayerCharacter::MulticastUpdateCanTurnBody_Implementation(bool newState)
{
	bUseControllerRotationYaw = newState;
	Mesh1P->DetachFromComponent(FDetachmentTransformRules(EDetachmentRule::KeepWorld, true));

	if (newState) {
		Mesh1P->AttachToComponent(FirstPersonCameraComponent, FAttachmentTransformRules(EAttachmentRule::KeepWorld, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, true), TEXT("GripPoint"));
		Mesh1P->SetRelativeLocationAndRotation(FVector(-0.5f, -4.4f, -155.7f), FRotator(1.9f, -19.19f, 5.2f));
	}
	else Mesh1P->AttachToComponent(GetCapsuleComponent(), FAttachmentTransformRules(EAttachmentRule::KeepWorld, true));
}

void AStealthMultiplayerCharacter::PrepareResetActiveAction2()
{
	GetWorldTimerManager().SetTimer(resetActiveAction2Handle, this, &AStealthMultiplayerCharacter::ResetActiveAction2, activeAction2ReuseDelay, false);
}

void AStealthMultiplayerCharacter::SetGrabbedObject(AActor * newGrabbedObject)
{
	grabbedObject = newGrabbedObject;
}

void AStealthMultiplayerCharacter::MulticastUpdateInventory_Implementation(EPickupItems newItemType, FInventory newItemData)
{
	//TODO maybe change to Client only because the other players dont seem to require this information
	//in case of change the Pickup() function might need to set these values also to have an correct reference on the server
	FInventory* value = inventory.Find(newItemType);
	if (value != nullptr) value->amount += newItemData.amount;
	else inventory.Add(newItemType, newItemData);
}

void AStealthMultiplayerCharacter::ServerUnlockTarget_Implementation(AActor * target, bool newLockState)
{
	if (target->GetClass()->ImplementsInterface(UInteractableInterface::StaticClass())) IInteractableInterface::Execute_Unlock(target, newLockState);
}

bool AStealthMultiplayerCharacter::ServerUnlockTarget_Validate(AActor * target, bool newLockState)
{
	return true;
}

void AStealthMultiplayerCharacter::OnFire()
{
	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	// try and play a firing animation if specified
	if (FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

void AStealthMultiplayerCharacter::ServerOnFire_Implementation()
{
	// try and fire a projectile
	if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			
			const FRotator SpawnRotation = GetControlRotation();
			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
			const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

			// spawn the projectile at the muzzle
			World->SpawnActor<AStealthMultiplayerProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
		}
	}
}

bool AStealthMultiplayerCharacter::ServerOnFire_Validate()
{
	return true;
}

void AStealthMultiplayerCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		if (grabbedObject != nullptr) {
			AGrabItem* obj = Cast<AGrabItem>(grabbedObject);
			if (obj->GetRequiredStrength() <= strength) {
				AddMovementInput(obj->GetMovementForwardVector(), Value);
			}
		}
		else AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AStealthMultiplayerCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		if (grabbedObject != nullptr) {
			AGrabItem* obj = Cast<AGrabItem>(grabbedObject);
			if (obj->GetRequiredStrength() <= strength) {
				AddMovementInput(obj->GetMovementRightVector(), Value);
			}
		}
		else AddMovementInput(GetActorRightVector(), Value);
	}
}

void AStealthMultiplayerCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AStealthMultiplayerCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AStealthMultiplayerCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	GetWorldTimerManager().ClearTimer(resetActiveAction1Handle);
	GetWorldTimerManager().ClearTimer(resetActiveAction2Handle);
}

void AStealthMultiplayerCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AStealthMultiplayerCharacter, canUseActiveAction1);
	DOREPLIFETIME(AStealthMultiplayerCharacter, canUseActiveAction2);
	DOREPLIFETIME(AStealthMultiplayerCharacter, canTurnBody);
	DOREPLIFETIME(AStealthMultiplayerCharacter, grabbedObject);
}
// Fill out your copyright notice in the Description page of Project Settings.

#include "KickListWidget.h"
#include "StealthMultiplayer.h"
#include "KickPlayerUserWidget.h"

void UKickListWidget::UpdateWindow_Implementation(const TArray<FPlayerInfo>& playersInfo)
{
	kickListRef->ClearChildren();
	for (int i = 0; i < playersInfo.Num(); i++)
	{
		if (i == 0) continue;
		kickButtonRef = CreateWidget<UUserWidget>(GetWorld(), wPlayerKickButton);
		UKickPlayerUserWidget* ref = Cast<UKickPlayerUserWidget>(kickButtonRef);
		ref->playerSettings = playersInfo[i];
		ref->connectedPlayerName = playersInfo[i].playerName;
		ref->connectedPlayerIcon = playersInfo[i].playerIcon;
		ref->connectedPlayerID = i;
		kickListRef->AddChild(kickButtonRef);
	}
}


bool UKickListWidget::UpdateWindow_Validate(const TArray<FPlayerInfo>& playersInfo)
{
	return true;
}
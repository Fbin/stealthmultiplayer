// Fill out your copyright notice in the Description page of Project Settings.

#include "HackableGateLock.h"
#include "Components/StaticMeshComponent.h"
#include "UnrealNetwork.h"

AHackableGateLock::AHackableGateLock()
{
	lockHandlesRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Lockhandles Root"));
	lockHandlesRoot->SetupAttachment(GetMesh());
}

void AHackableGateLock::BeginPlay()
{
	Super::BeginPlay();
	
	if (HasAuthority()) {
		for (int i = 0; i < lockHandles.Num(); i++)
		{
			lockHandlePositions[i] = lockHandles[i]->RelativeLocation;
		}
	}
}

void AHackableGateLock::MoveForward(float value)
{
	if (value != 0.0f && !isHacking) {
		if (lockHandles.Num() > 0) {
			FVector newLocation = lockHandlePositions[activeHandleIndex] + lockHandlesInfo[activeHandleIndex].lockHandleMoveDirectionForward * moveSpeed * FMath::CeilToFloat(value);
			if (IsHandleInLimits(newLocation, lockHandlesInfo[activeHandleIndex].lockHandleMoveDirectionForward)) {
				isHacking = true;
				ServerUpdateLockHandlesPosition(newLocation);
				//GetWorldTimerManager().SetTimer(hackingHandle, this, &AHackableGateLock::EnableLockMovement, handleMoveDelay, false);
				//TODO ClientUpdateLockHandles() instead to Multicast?
			}
		}
	}
}

void AHackableGateLock::MoveRight(float value)
{
	if (value != 0.0f && !isHacking) {
		if (lockHandles.Num() > 0) {
			FVector newLocation = lockHandlePositions[activeHandleIndex] + lockHandlesInfo[activeHandleIndex].lockHandleMoveDirectionRight * moveSpeed * FMath::CeilToFloat(value);
			if (IsHandleInLimits(newLocation, lockHandlesInfo[activeHandleIndex].lockHandleMoveDirectionRight)) {
				isHacking = true;
				ServerUpdateLockHandlesPosition(newLocation);
				//TODO ClientUpdateLockHandles() instead to Multicast?
			}
		}
	}
}

void AHackableGateLock::InitiateRespossess()
{
	//TODO only allow repossess of player if riddle is solved?
	//currently effects will be played whenever the correct password is entered, but unlock will only be triggered upon repossess
	//maybe automatic repossess upon correct password?
	//Unlock_Implementation(CheckPassword() || !isLocked);
	ServerTryUnlock(this);
	Super::InitiateRespossess();
}

void AHackableGateLock::InitiateHackOtherObject()
{
	ServerUpdateActiveHandleIndex();
}

void AHackableGateLock::ServerUpdateActiveHandleIndex_Implementation()
{
	if (lockHandles.Num() > 0) {
		activeHandleIndex++;
		activeHandleIndex %= lockHandles.Num();
	}
}

bool AHackableGateLock::ServerUpdateActiveHandleIndex_Validate()
{
	return true;
}

void AHackableGateLock::ServerUpdateLockHandlesPosition_Implementation(FVector newLocation)
{
	lockHandlePositions[activeHandleIndex] = newLocation;
	ClientUpdateLockHandlesPosition(CheckPassword(), newLocation);
}

bool AHackableGateLock::ServerUpdateLockHandlesPosition_Validate(FVector newLocation)
{
	return true;
}

void AHackableGateLock::ClientUpdateLockHandlesPosition_Implementation(bool passwordCorrect, FVector newLocation)
{
	lockHandles[activeHandleIndex]->SetRelativeLocation(newLocation);
	GetWorldTimerManager().SetTimer(hackingHandle, this, &AHackableGateLock::EnableLockMovement, handleMoveDelay, false);
	//if(passwordCorrect) //TODO play effects
}

bool AHackableGateLock::ClientUpdateLockHandlesPosition_Validate(bool passwordCorrect, FVector newLocation)
{
	return true;
}

void AHackableGateLock::ServerTryUnlock_Implementation(AActor* target)
{
	Execute_Unlock(this, CheckPassword());
}

bool AHackableGateLock::ServerTryUnlock_Validate(AActor* target)
{
	return true;
}

bool AHackableGateLock::CheckPassword()
{
	for (int i = 0; i < lockHandles.Num(); i++)
	{
		if (!lockHandlePositions[i].Equals(lockPassword[i], errorZone)) return false;
	}
	return true;
}

bool AHackableGateLock::IsHandleInLimits(FVector newLocation, FVector direction)
{
	float checkMax = FVector::DotProduct((lockHandlesInfo[activeHandleIndex].handleLimitMax - newLocation), direction);
	float checkMin = FVector::DotProduct((newLocation - lockHandlesInfo[activeHandleIndex].handleLimitMin), direction);
	return checkMax >= 0.0f && checkMin >= 0.0f;
}

void AHackableGateLock::EnableLockMovement()
{
	isHacking = false;
}

void AHackableGateLock::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AHackableGateLock, activeHandleIndex);
	DOREPLIFETIME(AHackableGateLock, lockHandlePositions);
}
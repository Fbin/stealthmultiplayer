// Fill out your copyright notice in the Description page of Project Settings.

#include "Treasure.h"
#include "StealthMultiplayerCharacter.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATreasure::ATreasure()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootSceneComponent"));
	visualComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("staticMesh"));
	visualComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	visualComponent->SetupAttachment(RootComponent);

	colliderComponent = CreateDefaultSubobject<USphereComponent>(TEXT("collider"));
	colliderComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	colliderComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	colliderComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	//TODO change to Interactable channel
	colliderComponent->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	colliderComponent->SetupAttachment(RootComponent);
	colliderComponent->OnComponentBeginOverlap.AddDynamic(this, &ATreasure::OnOverlapBegin);

	//SetReplicates(true);
}

// Called when the game starts or when spawned
void ATreasure::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATreasure::PlayEffects()
{
	//TODO might need a multicast function
	UGameplayStatics::SpawnEmitterAtLocation(this, pickupEffect, GetActorLocation());
}

//void ATreasure::NotifyActorBeginOverlap(AActor * OtherActor)
//{
//	Super::NotifyActorBeginOverlap(OtherActor);
//
//	/*PlayEffects();*/
//}

void ATreasure::PickUp_Implementation(AActor* user)
{
	PlayEffects();
	if (HasAuthority()) {
		AStealthMultiplayerCharacter* player = Cast<AStealthMultiplayerCharacter>(user);
		if (player) {
			//TODO check if class is correct or needs something else
			player->MulticastUpdateInventory(itemType, FInventory(ATreasure::StaticClass(), visualComponent, colliderComponent, pickupEffect, gateRef, amount));
		}
		Destroy();
	}
}

void ATreasure::Drop_Implementation()
{
}

void ATreasure::Interact_Implementation(AActor * user)
{
	Execute_PickUp(this, user);
}

void ATreasure::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
}
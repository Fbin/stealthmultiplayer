// Fill out your copyright notice in the Description page of Project Settings.

#include "Door.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Types.h"
#include "StealthMultiplayerCharacter.h"
#include "UnrealNetwork.h"

// Sets default values
ADoor::ADoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = root;

	mainDoor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Main Door"));
	mainDoor->SetupAttachment(RootComponent);

	secondDoor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Second Door"));
	secondDoor->SetupAttachment(RootComponent);

	unlockColliderComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Unlock Collider Component"));
	unlockColliderComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	unlockColliderComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	unlockColliderComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	unlockColliderComponent->SetupAttachment(RootComponent);
	unlockColliderComponent->OnComponentBeginOverlap.AddDynamic(this, &ADoor::OnOverlapBegin);

	SetReplicateMovement(true);
	SetReplicates(true);
}

// Called when the game starts or when spawned
void ADoor::BeginPlay()
{
	Super::BeginPlay();
	
	mainDefaultLocation = mainDoor->RelativeLocation;
	secondDefaultLocation = secondDoor->RelativeLocation;
	//startRotation = mainDoor->RelativeRotation;
}

bool ADoor::GetIsLocked()
{
	bool lockBlocked = false;
	for (int i = 0; i < locks.Num(); i++)
	{
		if (locks[i]->GetClass()->ImplementsInterface(UInteractableInterface::StaticClass())) {
			if (!IInteractableInterface::Execute_GetIsUnlocked(locks[i])) {
				lockBlocked = true;
				break;
			}
		}
	}

	return isLocked || lockBlocked;
}

// Called every frame
void ADoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (currentTime < openingTime) {
		currentTime += DeltaTime;
		float alpha = currentTime / openingTime;
		
		FVector newLocationMain, newLocationSecond;
		FRotator newRotationMain, newRotationSecond;
		switch (doorOpenType)
		{
		case EDoorType::Slide:
			if (isOpening) {
				newLocationMain = FMath::InterpEaseInOut(mainDefaultLocation, mainDefaultLocation + targetOffsetLocationMain, alpha, 2.0f);
				newLocationSecond = FMath::InterpEaseInOut(secondDefaultLocation, secondDefaultLocation + targetOffsetLocationSecond, alpha, 2.0f);
			}
			else {
				newLocationMain = FMath::InterpEaseInOut(mainDefaultLocation + targetOffsetLocationMain, mainDefaultLocation, alpha, 2.0f);
				newLocationSecond = FMath::InterpEaseInOut(secondDefaultLocation + targetOffsetLocationSecond, secondDefaultLocation, alpha, 2.0f);
			}

			mainDoor->SetRelativeLocation(newLocationMain);
			secondDoor->SetRelativeLocation(newLocationSecond);
			break;
		case EDoorType::Swing:
			if (isOpening) {
				newRotationMain = FMath::InterpEaseInOut(mainDefaultRotation, mainDefaultRotation + targetOffsetRotationMain, alpha, 2.0);
				newRotationSecond = FMath::InterpEaseInOut(secondDefaultRotation, secondDefaultRotation + targetOffsetRotationSecond, alpha, 2.0);
			}
			else {
				newRotationMain = FMath::InterpEaseInOut(mainDefaultRotation + targetOffsetRotationMain, mainDefaultRotation, alpha, 2.0);
				newRotationSecond = FMath::InterpEaseInOut(secondDefaultRotation + targetOffsetRotationSecond, secondDefaultRotation, alpha, 2.0);
			}

			mainDoor->SetRelativeRotation(newRotationMain);
			secondDoor->SetRelativeRotation(newRotationSecond);
			break;
		default:
			break;
		}
	}
}

void ADoor::Interact_Implementation(AActor * user)
{
	if (!GetIsLocked()) {
		if (currentTime >= openingTime) currentTime = 0.0f;
		else currentTime = openingTime - currentTime;

		isOpening = !isOpening;
	}
}

void ADoor::Unlock_Implementation(bool unlockInteractable)
{
	isLocked = !unlockInteractable;
}

void ADoor::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		//TODO will be used if moveable objects need to be placed as key
		//if (key != nullptr) {
			AStealthMultiplayerCharacter* player = Cast<AStealthMultiplayerCharacter>(OtherActor);
			if (player) {
				for (auto& Elem : player->inventory) {
					if (Elem.Value.gateRef == this) {
						//TODO maybe remove item and/or add use amount
						player->ServerUnlockTarget(this, true);
					}
				}
			}
		//}
	}
}

#if WITH_EDITOR
bool ADoor::CanEditChange(const UProperty * InProperty) const
{
	const bool ParentVal = Super::CanEditChange(InProperty);

	if (InProperty->GetFName() == GET_MEMBER_NAME_CHECKED(ADoor, targetOffsetLocationMain) || InProperty->GetFName() == GET_MEMBER_NAME_CHECKED(ADoor, targetOffsetLocationSecond))
	{
		return doorOpenType == EDoorType::Slide;
	}

	if (InProperty->GetFName() == GET_MEMBER_NAME_CHECKED(ADoor, targetOffsetRotationMain) || InProperty->GetFName() == GET_MEMBER_NAME_CHECKED(ADoor, targetOffsetRotationSecond))
	{
		return doorOpenType == EDoorType::Swing;
	}

	return ParentVal;
}
#endif

void ADoor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ADoor, isLocked);
	DOREPLIFETIME(ADoor, currentTime);
	DOREPLIFETIME(ADoor, isOpening);
}
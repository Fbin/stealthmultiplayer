// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "StealthMultiplayerGameMode.h"
#include "StealthMultiplayerHUD.h"
#include "StealthMultiplayerGameState.h"
#include "StealthMultiplayerPlyrController.h"
#include "StealthMultiplayerCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "GameFramework/PlayerState.h"

AStealthMultiplayerGameMode::AStealthMultiplayerGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AStealthMultiplayerHUD::StaticClass();
	GameStateClass = AStealthMultiplayerGameState::StaticClass();
}

void AStealthMultiplayerGameMode::GameFinished(APawn * instigatorPawn, bool gameSuccessful)
{
	if (instigatorPawn) {
		//TODO maybe change to actual reference of object instead
		if (spectatingViewpointClass) {
			TArray<AActor*> foundActors;
			UGameplayStatics::GetAllActorsOfClass(this, spectatingViewpointClass, foundActors);

			if (foundActors.Num() > 0) {

				for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; it++) {
					AStealthMultiplayerPlyrController* playerController = Cast<AStealthMultiplayerPlyrController>(it->Get());
					if (playerController) {
						playerController->SetViewTargetWithBlend(foundActors[0], 0.5f, EViewTargetBlendFunction::VTBlend_EaseIn);
					}
				}
			}
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("spectatingViewpointClass is not set in gamemode"));
		}
	}

	AStealthMultiplayerGameState* gameState = GetGameState<AStealthMultiplayerGameState>();
	if (gameState) {
		gameState->MulticastOnGameFinished(instigatorPawn, gameSuccessful);
	}
}

void AStealthMultiplayerGameMode::RestartPlayerAtPlayerStart(AController* NewPlayer, AActor* StartSpot)
{
	if (NewPlayer == nullptr || NewPlayer->IsPendingKillPending())
	{
		return;
	}

	if (!StartSpot)
	{
		UE_LOG(LogGameMode, Warning, TEXT("RestartPlayerAtPlayerStart: Player start not found"));
		return;
	}

	FRotator SpawnRotation = StartSpot->GetActorRotation();

	UE_LOG(LogGameMode, Verbose, TEXT("RestartPlayerAtPlayerStart %s"), (NewPlayer && NewPlayer->PlayerState) ? *NewPlayer->PlayerState->GetPlayerName() : TEXT("Unknown"));

	if (MustSpectate(Cast<APlayerController>(NewPlayer)))
	{
		UE_LOG(LogGameMode, Verbose, TEXT("RestartPlayerAtPlayerStart: Tried to restart a spectator-only player!"));
		return;
	}

	if (NewPlayer->GetPawn() != nullptr)
	{
		// If we have an existing pawn, just use it's rotation
		SpawnRotation = NewPlayer->GetPawn()->GetActorRotation();
	}
	else if (GetDefaultPawnClassForController(NewPlayer) != nullptr)
	{
		// Try to create a pawn to use of the default class for this player
		FRotator StartRotation(ForceInit);
		StartRotation.Yaw = StartSpot->GetActorRotation().Yaw;
		FVector StartLocation = StartSpot->GetActorLocation();
		FTransform Transform = FTransform(StartRotation, StartLocation);

		FActorSpawnParameters SpawnInfo;
		SpawnInfo.Instigator = Instigator;
		SpawnInfo.ObjectFlags |= RF_Transient;	// We never want to save default player pawns into a map
		
		AStealthMultiplayerPlyrController* MyController = Cast<AStealthMultiplayerPlyrController>(NewPlayer);
		UClass* PawnClass;
		if (MyController) PawnClass = MyController->GetPlayerPawnClass();
		else PawnClass = GetDefaultPawnClassForController(NewPlayer);

		APawn* ResultPawn = GetWorld()->SpawnActor<APawn>(PawnClass, Transform, SpawnInfo);
		if (!ResultPawn)
		{
			UE_LOG(LogGameMode, Warning, TEXT("SpawnDefaultPawnAtTransform: Couldn't spawn Pawn of type %s at %s"), *GetNameSafe(PawnClass), *Transform.ToHumanReadableString());
		}

		if (useDefaultClass) {
			NewPlayer->SetPawn(SpawnDefaultPawnFor(NewPlayer, StartSpot));
		}
		else {
			NewPlayer->SetPawn(ResultPawn);
		}
	}

	if (NewPlayer->GetPawn() == nullptr)
	{
		NewPlayer->FailedToSpawnPawn();
	}
	else
	{
		// Tell the start spot it was used
		InitStartSpot(StartSpot, NewPlayer);

		FinishRestartPlayer(NewPlayer, SpawnRotation);
	}
}

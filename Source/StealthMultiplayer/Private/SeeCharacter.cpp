// Fill out your copyright notice in the Description page of Project Settings.

#include "SeeCharacter.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"
#include "HackablePawn.h"
#include "GameFramework/PlayerController.h"
//necessary for doreplifetime
#include "UnrealNetwork.h"
#include "DrawDebugHelpers.h"

ASeeCharacter::ASeeCharacter()
{
	isDefaultClass = false;

	UCharacterMovementComponent* movementRef = Cast<UCharacterMovementComponent>(GetMovementComponent());
	movementRef->BrakingDecelerationFlying = movementRef->MaxAcceleration;
}

void ASeeCharacter::PrepareResetActiveAction2()
{
	GetWorldTimerManager().SetTimer(resetActiveAction2Handle, this, &ASeeCharacter::ResetActiveAction2, activeAction2ReuseDelay + 0.1f, false);
	DisableInput(Cast<APlayerController>(GetController()));
}

void ASeeCharacter::SetupPlayerInputComponent(UInputComponent * InputComponent)
{
	check(InputComponent);

	Super::SetupPlayerInputComponent(InputComponent);

}

void ASeeCharacter::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	if (IsLocallyControlled()) {
		/*if (!canUseActiveAction1 && LedgeCheck(GetCapsuleComponent()->GetScaledCapsuleHalfHeight(), true)) {
			ServerSetIsClimbing(false);
		}*/
		//if (isClimbing && Cast<UCharacterMovementComponent>(GetMovementComponent())->MovementMode == EMovementMode::MOVE_Walking) ServerSetIsClimbing(false);
		float sizeA = tanf(60.0f * PI / 180.0f) * GetCapsuleComponent()->GetScaledCapsuleHalfHeight() + 10.0f + GetCapsuleComponent()->GetScaledCapsuleRadius();
		float sizeB = tanf(60.0f * PI / 180.0f) * GetCapsuleComponent()->GetScaledCapsuleHalfHeight() * 2.0f + 10.0f + GetCapsuleComponent()->GetScaledCapsuleRadius();
		FVector start = GetCapsuleComponent()->GetComponentLocation() - GetCapsuleComponent()->GetUpVector() * GetCapsuleComponent()->GetScaledCapsuleHalfHeight()/* + GetCapsuleComponent()->GetForwardVector() * GetCapsuleComponent()->GetScaledCapsuleRadius()*/;
		FVector startA = GetCapsuleComponent()->GetComponentLocation()/* + GetCapsuleComponent()->GetForwardVector() * GetCapsuleComponent()->GetScaledCapsuleRadius()*/;
		FVector startB = GetCapsuleComponent()->GetComponentLocation() + GetCapsuleComponent()->GetUpVector() * GetCapsuleComponent()->GetScaledCapsuleHalfHeight()/* + GetCapsuleComponent()->GetForwardVector() * GetCapsuleComponent()->GetScaledCapsuleRadius()*/;
		FVector end = start + GetCapsuleComponent()->GetForwardVector() * (10.0f + GetCapsuleComponent()->GetScaledCapsuleRadius());
		FVector endA = startA + GetCapsuleComponent()->GetForwardVector() * sizeA;
		FVector endB = startB + GetCapsuleComponent()->GetForwardVector() * sizeB;

		/*DrawDebugLine(GetWorld(), start, end, FColor::Red, false, 10.0f);
		DrawDebugLine(GetWorld(), startA, endA, FColor::Blue, false, 10.0f);
		DrawDebugLine(GetWorld(), startB, endB, FColor::Green, false, 10.0f);*/
	}
}

void ASeeCharacter::MoveForward(float Val)
{
	if (isClimbing) {
		if (Val < 0.0f) {
			if (GroundCheck()) {
				ServerSetIsClimbing(false);
				Super::MoveForward(Val);
			}
			else {
				FVector currentNormal = currentWall.ImpactNormal;
				currentNormal.Z = 0.0f;
				FVector rotateAxis = currentNormal.RotateAngleAxis(-90.0f, FVector(0.0f, 0.0f, 1.0f));
				FVector direction = currentNormal.RotateAngleAxis(90.0f, rotateAxis);
				AddMovementInput(/*GetActorUpVector()*/direction, Val);
			}
		}
		//if use just > allows player to watch the surrounding if there is no movement at all, >= instead drops the player
		else if (Val > 0.0f) {
			if (LedgeCheck(GetCapsuleComponent()->GetScaledCapsuleHalfHeight(), true) && LedgeCheck(-GetCapsuleComponent()->GetScaledCapsuleHalfHeight(), true)) {
				ServerSetIsClimbing(false);
				Super::MoveForward(Val);
			}
			else {
				FVector currentNormal = currentWall.ImpactNormal;
				currentNormal.Z = 0.0f;
				FVector rotateAxis = currentNormal.RotateAngleAxis(-90.0f, FVector(0.0f, 0.0f, 1.0f));
				FVector direction = currentWall.ImpactNormal.RotateAngleAxis(90.0f, rotateAxis);
				//DrawDebugLine(GetWorld(), GetActorLocation(), GetActorLocation() + direction * 100.0f, FColor::Red, false, 10.0f);
				AddMovementInput(/*GetActorUpVector()*/direction, Val);
			}
		}
	}
	else {
		if (Val > 0.0f) {
			if (!LedgeCheck()) {
				ServerSetIsClimbing(true);
			}
			else Super::MoveForward(Val);
		}
		else Super::MoveForward(Val);
	}
}

void ASeeCharacter::MoveRight(float Val)
{
	if (isClimbing) {
		if (Val != 0.0f) {
			if (LedgeCheck(GetCapsuleComponent()->GetScaledCapsuleHalfHeight(), true) && LedgeCheck(-GetCapsuleComponent()->GetScaledCapsuleHalfHeight(), true)) {
				ServerSetIsClimbing(false);
				Super::MoveRight(Val);
			}
			else {
				FVector currentNormal = currentWall.ImpactNormal;
				currentNormal.Z = 0.0f;
				FVector direction = currentNormal.RotateAngleAxis(-90.0f, FVector(0.0f, 0.0f, 1.0f));
				
				AddMovementInput(direction, Val);
			}
		}
	}
	else Super::MoveRight(Val);
}

void ASeeCharacter::ActiveAction1()
{
		if (isClimbing) {
			if (canUseActiveAction1) {
				Super::ActiveAction1();
				//if (didUseWallJump) {
				//	//grabs to wall again
				//	if (!LedgeCheck()) {
				//		//didUseWallJump = false;
				//		ServerSetIsClimbing(true);
				//	}
				//}
				//else {
					//TODO same as in MoveForward(), use the new axis to determine jump orientation
					//maybe instead of forward/up allow jump along movement direction e.g. velocity.normalize * climbJumpForce
					//GetMovementComponent()->Velocity.Z = FMath::Max(GetMovementComponent()->Velocity.Z, climbJumpForce);
					float launchVel = FMath::Max(GetMovementComponent()->Velocity.Size(), climbJumpForce);
					FVector dir = GetMovementComponent()->Velocity;
					if (dir.Size() == 0.0f) dir = FVector::UpVector;
					else dir.Normalize();
					//LaunchCharacter(dir * launchVel, false, false);
					ServerJump(dir, launchVel);
					PrepareResetActiveAction1();
				//}
			}
		}
		else {
			if (didUseWallJump) {
				//grabs to wall again
				if (!LedgeCheck()) {
					//didUseWallJump = false;
					ServerSetIsClimbing(true);
				}
			}
		}
}

void ASeeCharacter::ActiveAction2()
{
	if (canUseActiveAction2) {
		Super::ActiveAction2();

		FHitResult hitResult;
		FVector start = GetFirstPersonCameraComponent()->GetComponentLocation();
		FVector end = start + GetFirstPersonCameraComponent()->GetForwardVector() * hackingTraceLength;
		ECollisionChannel traceChannel = ECollisionChannel::ECC_Visibility;
		GetWorld()->LineTraceSingleByChannel(hitResult, start, end, traceChannel);
		if (Cast<AHackablePawn>(hitResult.Actor)) {
			if (Cast<AHackablePawn>(hitResult.Actor)->CanBeHacked()) {
				hackingTarget = Cast<AHackablePawn>(hitResult.Actor);
				hackingTarget->SetHacker(this);
				Cast<APlayerController>(GetController())->SetViewTargetWithBlend(hackingTarget, activeAction2ReuseDelay, EViewTargetBlendFunction::VTBlend_EaseInOut, 2.0f);
				PrepareResetActiveAction2();
			}
		}
		else PrepareHackFailed();
	}
}

void ASeeCharacter::ResetActiveAction2()
{
	Super::ResetActiveAction2();
	AController* controller = GetController();
	//TODO check if more needs to be moved to server and replicated the variables
	EnableInput(Cast<APlayerController>(controller));
	ServerPossess(controller, hackingTarget);
	//hackingTarget->EnableInput(Cast<APlayerController>(controller));
	//TODO turn off the HUD
}

void ASeeCharacter::PrepareHackFailed()
{
	GetWorldTimerManager().SetTimer(resetActiveAction2Handle, this, &ASeeCharacter::HackFailed, activeAction2ReuseDelay, false);
}

void ASeeCharacter::HackFailed()
{
	GetWorldTimerManager().ClearTimer(resetActiveAction2Handle);
	ServerResetActiveAction2();
}

bool ASeeCharacter::GroundCheck()
{
	FHitResult hitResult;
	FVector start = GetCapsuleComponent()->GetComponentLocation();
	FVector end = start - GetCapsuleComponent()->GetUpVector() * (GetCapsuleComponent()->GetScaledCapsuleHalfHeight() + 10.0f);
	ECollisionChannel traceChannel = ECollisionChannel::ECC_Visibility;
	GetWorld()->LineTraceSingleByChannel(hitResult, start, end, traceChannel);

	if (hitResult.bBlockingHit) return hitResult.Actor == currentWall.Actor ? false : true;
	else return hitResult.bBlockingHit;
}

bool ASeeCharacter::LedgeCheck(float offsetZ/* = 0.0f*/, bool additionalLength/* = false*/)
{
	FHitResult hitResult;
	FCollisionQueryParams TraceParams(FName(TEXT("LedgeCheck")), true, this);
	FVector start = GetCapsuleComponent()->GetComponentLocation() - GetCapsuleComponent()->GetUpVector() * offsetZ/* + GetCapsuleComponent()->GetForwardVector() * GetCapsuleComponent()->GetScaledCapsuleRadius()*/;
	float baseTraceLength = maxClimbDistance + GetCapsuleComponent()->GetScaledCapsuleRadius();
	//TODO create it more dynamic
	if (additionalLength) {
		//uppermost check/head check
		if (offsetZ < 0.0f) baseTraceLength += tanf(maxClimbWallBentAngle * PI / 180.0f) * /*GetCapsuleComponent()->GetScaledCapsuleHalfHeight()*/-offsetZ * 2.0f;
	}
	//center check
	else baseTraceLength += tanf(maxClimbWallBentAngle * PI / 180.0f) * GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	float traceLength = additionalLength ? baseTraceLength / cosf(maxClimbAngle * PI / 180.0f) : baseTraceLength;

	FVector end = start + GetCapsuleComponent()->GetForwardVector() * traceLength;
	ECollisionChannel traceChannel = ECollisionChannel::ECC_Visibility;
	GetWorld()->LineTraceSingleByChannel(hitResult, start, end, traceChannel, TraceParams);

	//check if the center check hit distance is too far, then do lowermost check
	if (!additionalLength && hitResult.Distance > maxClimbDistance + GetCapsuleComponent()->GetScaledCapsuleRadius()) {
		start -= GetCapsuleComponent()->GetUpVector() * GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
		end = start + GetCapsuleComponent()->GetForwardVector() * (maxClimbDistance + GetCapsuleComponent()->GetScaledCapsuleRadius());
		GetWorld()->LineTraceSingleByChannel(hitResult, start, end, traceChannel, TraceParams);
	}
	//DrawDebugLine(GetWorld(), start, end, FColor::Red, false, 10.0f);
	if (hitResult.bBlockingHit) currentWall = hitResult;
	else currentWall = FHitResult();

	return !hitResult.bBlockingHit;
}

void ASeeCharacter::ServerPossess_Implementation(AController * controllerForPossess, APawn * target)
{
	controllerForPossess->Possess(target);
}

bool ASeeCharacter::ServerPossess_Validate(AController * controllerForPossess, APawn * target)
{
	return true;
}

void ASeeCharacter::ServerSetIsClimbing_Implementation(bool newClimbing)
{
	didUseWallJump = false;
	if (newClimbing) {
		Cast<UCharacterMovementComponent>(GetMovementComponent())->SetMovementMode(EMovementMode::MOVE_Flying);
		isClimbing = newClimbing;
	}
	else {
		Cast<UCharacterMovementComponent>(GetMovementComponent())->SetMovementMode(EMovementMode::MOVE_Walking);
		isClimbing = newClimbing;
	}
}

bool ASeeCharacter::ServerSetIsClimbing_Validate(bool newClimbing)
{
	return true;
}

void ASeeCharacter::ServerJump_Implementation(FVector direction, float force)
{
	//Cast<UCharacterMovementComponent>(GetMovementComponent())->SetMovementMode(EMovementMode::MOVE_Walking);
	didUseWallJump = true;
	isClimbing = false;
	LaunchCharacter(direction * force, false, false);
}

bool ASeeCharacter::ServerJump_Validate(FVector direction, float force)
{
	return true;
}

void ASeeCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASeeCharacter, isClimbing);
	DOREPLIFETIME(ASeeCharacter, didUseWallJump);
}
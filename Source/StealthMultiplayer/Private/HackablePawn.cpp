// Fill out your copyright notice in the Description page of Project Settings.

#include "HackablePawn.h"
#include "SeeCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/BoxComponent.h"
#include "GameFramework/PlayerController.h"
#include "UnrealNetwork.h"

// Sets default values
AHackablePawn::AHackablePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	//TODO setup collision for detection of hacker in range
	RootComponent = SphereComponent;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetCollisionProfileName(TEXT("Pawn"));
	Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	Mesh->SetupAttachment(SphereComponent);
	/*Mesh->bCastDynamicShadow = false;
	Mesh->CastShadow = false;*/

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(Mesh);
	//CameraComponent->bUsePawnControlRotation = true;

	unlockColliderComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Unlock Collider Component"));
	unlockColliderComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	unlockColliderComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	unlockColliderComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	unlockColliderComponent->SetupAttachment(RootComponent);
	unlockColliderComponent->OnComponentBeginOverlap.AddDynamic(this, &AHackablePawn::OnOverlapBegin);

	SetReplicateMovement(true);
	SetReplicates(true);
}

// Called when the game starts or when spawned
void AHackablePawn::BeginPlay()
{
	Super::BeginPlay();
	
	if (materialParameterCollectionRef != nullptr) materialParameterCollection = GetWorld()->GetParameterCollectionInstance(materialParameterCollectionRef);
}

void AHackablePawn::MoveForward(float value)
{
	float newAngle = currentUpAngle + value;
	if (newAngle < maxUpAngle && newAngle > -maxDownAngle) {
		currentUpAngle = newAngle;
		Mesh->AddRelativeRotation(FRotator(value, 0.0f, 0.0f));
	}
	//check for a way to discern between keyboard and controller input (AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());)
}

void AHackablePawn::MoveRight(float value)
{
	float newAngle = currentRightAngle + value;
	if (newAngle < maxRightAngle && newAngle > -maxLeftAngle) {
		currentRightAngle = newAngle;
		Mesh->AddWorldRotation(FRotator(0.0f, value, 0.0f));
	}
	//check for a way to discern between keyboard and controller input (AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());)
}

void AHackablePawn::InitiateRespossess()
{
	Cast<APlayerController>(GetController())->SetViewTargetWithBlend(hackerRef, hackerRef->activeAction2ActiveTime, EViewTargetBlendFunction::VTBlend_EaseInOut, 2.0f);
	GetWorldTimerManager().SetTimer(hackingHandle, this, &AHackablePawn::RepossessPlayer, hackerRef->activeAction2ActiveTime + 0.1f, false);
	DisableInput(Cast<APlayerController>(GetController()));
}

void AHackablePawn::RepossessPlayer()
{
	AController* controller = GetController();
	//TODO check if more needs to be moved to server and replicated the variables
	EnableInput(Cast<APlayerController>(controller));
	ServerPossess(controller, hackerRef);
	//hackerRef->EnableInput(Cast<APlayerController>(controller));
	SetHacker(nullptr);
	//TODO turn on HUD
}

void AHackablePawn::InitiateHackOtherObject()
{
	if (!isHacking) {
		isHacking = true;
		ReceiveHackOtherObject();

		FHitResult hitResult;
		FVector start = GetCameraComponent()->GetComponentLocation();
		FVector end = start + GetCameraComponent()->GetForwardVector() * hackingTraceLength;
		ECollisionChannel traceChannel = ECollisionChannel::ECC_Visibility;
		FCollisionQueryParams params = FCollisionQueryParams();
		params.AddIgnoredActor(this);
		GetWorld()->LineTraceSingleByChannel(hitResult, start, end, traceChannel, params);
		if (Cast<AHackablePawn>(hitResult.Actor)) {
			if (Cast<AHackablePawn>(hitResult.Actor)->CanBeHacked()) {
				hackingTarget = Cast<AHackablePawn>(hitResult.Actor);
				hackingTarget->SetHacker(hackerRef);

				Cast<APlayerController>(GetController())->SetViewTargetWithBlend(hackingTarget, hackerRef->activeAction2ActiveTime, EViewTargetBlendFunction::VTBlend_EaseInOut, 2.0f);
				GetWorldTimerManager().SetTimer(hackingHandle, this, &AHackablePawn::HackOtherObject, hackerRef->activeAction2ActiveTime + 0.1f, false);
				DisableInput(Cast<APlayerController>(GetController()));
			}
			else isHacking = false;
		}
		else isHacking = false;
	}
}

void AHackablePawn::HackOtherObject()
{
	isHacking = false;
	AController* controller = GetController();
	//TODO check if more needs to be moved to server and replicated the variables
	EnableInput(Cast<APlayerController>(controller));
	ServerPossess(controller, hackingTarget);
	//hackingTarget->EnableInput(Cast<APlayerController>(controller));
	SetHacker(nullptr);
}

void AHackablePawn::ServerUnlock_Implementation(bool newUnlockState)
{
	IInteractableInterface::Execute_Unlock(this, newUnlockState);
}

bool AHackablePawn::ServerUnlock_Validate(bool newUnlockState)
{
	return true;
}

// Called every frame
void AHackablePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (hackerRef == nullptr) {
		//TODO do animation
	}
	else {
		//TODO check if possible hacktarget is in view and then highlight it
	}
}

// Called to bind functionality to input
void AHackablePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("ActiveAction1", IE_Pressed, this, &AHackablePawn::InitiateRespossess);
	PlayerInputComponent->BindAction("ActiveAction2", IE_Pressed, this, &AHackablePawn::InitiateHackOtherObject);

	PlayerInputComponent->BindAxis("MoveForward", this, &AHackablePawn::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AHackablePawn::MoveRight);
}

void AHackablePawn::SetHacker(ASeeCharacter * newHacker)
{
	hackerRef = newHacker;
}

void AHackablePawn::Unlock_Implementation(bool unlockInteractable)
{
	isLocked = !unlockInteractable;
}

bool AHackablePawn::GetIsUnlocked_Implementation()
{
	bool lockBlocked = false;
	for (int i = 0; i < locks.Num(); i++)
	{
		if (locks[i]->GetClass()->ImplementsInterface(UInteractableInterface::StaticClass())) {
			if (!IInteractableInterface::Execute_GetIsUnlocked(locks[i])) {
				lockBlocked = true;
				break;
			}
		}
	}

	return !(isLocked || lockBlocked);
}

bool AHackablePawn::CanBeHacked()
{
	for (int i = 0; i < locks.Num(); i++)
	{
		if (locks[i]->GetClass()->ImplementsInterface(UInteractableInterface::StaticClass())) {
			if (!IInteractableInterface::Execute_GetIsUnlocked(locks[i])) return false;
		}
	}
	return true;
}

void AHackablePawn::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		//TODO will be used if moveable objects need to be placed as key
		//if (key != nullptr) {
			AStealthMultiplayerCharacter* player = Cast<AStealthMultiplayerCharacter>(OtherActor);
			if (player) {
				for (auto& Elem : player->inventory) {
					if (Elem.Value.gateRef == this) {
						//TODO maybe remove item and/or add use amount
						player->ServerUnlockTarget(this, true);
					}
				}
			}
		//}
	}
}

void AHackablePawn::ServerPossess_Implementation(AController * controllerForPossess, APawn * target)
{
	controllerForPossess->Possess(target);
}

bool AHackablePawn::ServerPossess_Validate(AController * controllerForPossess, APawn * target)
{
	return true;
}

void AHackablePawn::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AHackablePawn, isLocked);
}
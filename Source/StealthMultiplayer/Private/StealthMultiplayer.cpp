// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "StealthMultiplayer.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, StealthMultiplayer, "StealthMultiplayer" );
DEFINE_LOG_CATEGORY(LobbyLog);
DEFINE_LOG_CATEGORY(StealthIntanceLog);
// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class StealthMultiplayer : ModuleRules
{
	public StealthMultiplayer(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicIncludePaths.AddRange(new string[] { "StealthMultiplayer/Public" });

		PrivateIncludePaths.Add("StealthMultiplayer/Private");

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "AIModule", "UMG", "OnlineSubsystem", "OnlineSubsystemUtils", "NavigationSystem" });

		PrivateDependencyModuleNames.AddRange(new string[] { "CableComponent" });

		//TODO check if actually needed
		DynamicallyLoadedModuleNames.Add("OnlineSubsystemNull");
	}
}
